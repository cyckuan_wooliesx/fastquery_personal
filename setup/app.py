#!/usr/bin/env python3

import yaml
from importhelp import importlist
m = "generate_key encrypt_message decrypt_message"
__all__ = importlist.load_modules("crypt", m, __file__, globals())


def encrypt_list(password, secrets):

    key = generate_key(password)
    print("key : " + key.decode('utf-8'))

    with open(key, 'w') as f:
        f.write(key)

    for s in secrets:
        encrypted = encrypt_message(s, key)
        print(s)
        print(encrypted)
        print()


def encrypt_file(password, config_in, config_out):

    key = generate_key(password)
    print("key : " + key.decode('utf-8'))

    with open(config_in, 'r') as config_file:
        cfg = yaml.load(config_file, Loader=yaml.FullLoader)

    new_cfg = dict()
    for k in cfg:
        new_cfg[k] = encrypt_message(cfg[k], key)
        print('\n'.join([k, cfg[k], new_cfg[k]]))

    with open(config_out, 'w') as config_file:
        yaml.dump(new_cfg, config_file, default_flow_style=False)


def decrypt_file(password, config_yaml):

    key = generate_key(password)
    print("key : " + key.decode('utf-8'))

    with open(config_yaml, 'r') as db_config_file:
        cfg = yaml.load(db_config_file, Loader=yaml.FullLoader)

    for k in cfg:
        print(k)
        print(cfg[k])
        print(decrypt_message(cfg[k], key))
