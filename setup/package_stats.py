#!/usr/bin/env python3

from platform import python_version
package_list = [
    "autopep8"
]

print(python_version())

for p in package_list:
    print(p)
    mod = __import__(p)
    print(mod.__file__)
