#!/usr/bin/env python3

log_path = 'logs'
ddl_output_path = 'ddl'
json_output_path = 'json'
stop_code_path = 'stop_code.txt'



# syspath

import syspath
syspath.default_lib()



# dependent packages

import os
import argparse
import redshift
import gcloud
import util

from multiprocessing import Process
from datetime import datetime, timedelta
from sty import fg, bg, ef, rs, Rule



# class

class carryall(object):

  def __init__(self, **kwargs):
    self.__dict__.update(kwargs)

  def set(self, **kwargs):
    self.__dict__.update(kwargs)



# utility functions

def read_params(config_file):
  with open(config_file,'r') as f:
    tables = f.read().splitlines()

  tables = [t.strip().split('|') for t in tables if len(t.strip()) > 0]
  return tables

def stop_code(filename):

  if os.path.exists(filename):
    with open(filename) as f:
      c = f.read().strip()
      if c.isnumeric():
        c = int(c)
      else:
        c = 0
  else:
    c = 0

  return c



# schema extract convert

def process_createschema(ca):

  print(ca.schemaname)

  # get redshift table ddl
  ddl = ca.r_connection.extract_ddl(ca.schemaname, ca.src_tablename)
  ddl = '\n'.join(ddl['df']['ddl'].tolist())

  print(ddl)

  # write ddl to file
  with open(ddl_output_path + '/'+ ca.schemaname + '.' + ca.src_tablename + '.sql', "w") as text_file:
    text_file.write(ddl)

  # convert ddl to bigquery schema json
  json_schema = ca.r_connection.convert_ddl(ddl)

  print(json_schema)

  # write json to file
  with open(json_output_path + '/'+ ca.schemaname + '.' + ca.src_tablename + '.json', "w") as text_file:
    text_file.write(json_schema)


# create tables

def process_createtable(ca):

  bq_schematable = ca.dataset + '.' + ca.dest_tablename
  rs_schematable = ca.schemaname + '.' + ca.src_tablename
  rs_schemapath = json_output_path + '/' + rs_schematable + '.json'

  ca.g_connection.bq_delete_table(bq_schematable)
  ca.g_connection.bq_create_table(bq_schematable, rs_schemapath)


# unload

def process_unload(ca):

  sql = ca.r_connection.unload_sql(ca.schemaname, ca.src_tablename, ca.partition, ca.whereclause)

  ca.r_connection.run_sql_df(sql)


# transfer

def process_transfer(ca):

  config = ca.g_connection.get_config()

  src = '/'.join([config['s3repo'], config['dataprefix'], ca.schemaname, ca.src_tablename, ca.partition]) + '/'
  dest = '/'.join([config['gsrepo'], config['dataprefix'], ca.schemaname, ca.src_tablename, ca.partition]) + '/'

  ca.g_connection.rsync(src, dest)


# load

def process_load(ca):

  config = ca.g_connection.get_config()

  rs_schematable = ca.schemaname + '.' + ca.src_tablename
  rs_schemapath = json_output_path + '/' + rs_schematable + '.json'
  datafiles = '/'.join([config['gsrepo'], config['dataprefix'], ca.schemaname, ca.src_tablename, ca.partition, '*.gz'])

  ca.g_connection.load_csv_bq(ca.dataset, ca.dest_tablename, ca.schemaname, ca.src_tablename, datafiles, rs_schemapath, ca.loadmode)





# process caller

def call_process(ca, async_mode=0):

  print(fg.green + '\n' + ca.src_tablename + '\n' + ca.partition + '\n' + fg.rs)
  util.logentry.execute(ca.logfile, ca.src_tablename + ' : ' + ca.partition)

  if async_mode > 0:
    p = Process(target=ca.process, args=(ca,))
    p.start()
  else:
    ca.process(ca)





# main

def pipeline(config_path, secrets_path, secrets_key, process_name, process, tablelist_path, schemaname='', dataset='', loadmode='noreplace', async_mode=0, partition_flag=1):

  # log file

  logfile = 'logs/' + process_name + '_' + os.path.basename(tablelist_path) + '.log'
  print(logfile)

  # redshift config

  r = redshift.class_connection.connection(config_path, secrets_path, secrets_key)
  r.connect()
  r_config = r.get_config()

  # gcloud config

  g = gcloud.class_connection.connection(config_path, secrets_path, secrets_key)
  g_config = g.get_config()

  # secrets

  s = g.get_secrets()

  # figlet title

  util.figlet_title.execute(r_config['titlefont'], process_name)

  # get table list

  tables = read_params(tablelist_path)

  # table iterator

  stop = 0

  for table in tables:

    src_tablename = table[0]


    # stop

    stop = stop_code(stop_code_path)
    if stop > 0:
      break


    # param list management

    if len(table) > 2:
      dest_tablename = table[1]
      partition = table[2]
      pl = partition.split(':')
      whereclause_template = table[3]
    elif len(table) > 1:
      dest_tablename = table[1]
      partition = datetime.today().strftime('%Y-%m-%d')
      pl = ['na']
      whereclause = ''
    else:
      dest_tablename = src_tablename
      partition = datetime.today().strftime('%Y-%m-%d')
      pl = ['na']
      whereclause = ''


    # date iterator

    if pl[0] == 'daterange' and partition_flag > 0:

      date_from = datetime.strptime(pl[1],'%Y-%m-%d')
      date_to = datetime.strptime(pl[2],'%Y-%m-%d')

      day = timedelta(days=1)

      print(pl)

      # iterative call

      while date_from <= date_to and stop == 0:

        partition = date_from.strftime('%Y-%m-%d')
        whereclause = whereclause_template.replace('$date',partition)

        ca = carryall(r_connection=r, g_connection=g, secrets=s, dataset=dataset, schemaname=schemaname, src_tablename=src_tablename, dest_tablename=dest_tablename, partition=partition, whereclause=whereclause, loadmode=loadmode, process=process, logfile=logfile)
        call_process(ca, async_mode)

        date_from = date_from + day

    else:

      # single call

      ca = carryall(r_connection=r, g_connection=g, secrets=s, dataset=dataset, schemaname=schemaname, src_tablename=src_tablename, dest_tablename=dest_tablename, partition=partition, whereclause=whereclause, loadmode=loadmode, process=process, logfile=logfile)
      call_process(ca, async_mode)


  if stop > 0:
    print(fg.red + 'STOPPED' + fg.cancel)


  return 0





