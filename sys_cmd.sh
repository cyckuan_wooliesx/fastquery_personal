#!/usr/env/bin bash

alias lll="ls -alt | less"
alias ds="sudo du -sh $1 | sort -h"

n() {
  if [[ -d $1 ]]; then
    cd $1
    ls
  else
    nano $1
  fi
}

nn() {
  if [ ! -f $1 ]; then
      echo '#!/usr/bin/env' > $1
      chmod +x $1
      nano $1
  fi
}

nnp() {
  if [ ! -f $1 ]; then
      echo '#!/usr/bin/env python3' > $1
      chmod +x $1
      nano $1
  fi
}

nnb() {
  if [ ! -f $1 ]; then
      echo '#!/usr/bin/env bash' > $1
      chmod +x $1
      nano $1
  fi
}

sl() {
  screen -ls
}

s() {
  sn=`screen -ls | grep Detached | cut -f2 | head -n 1`
  screen -R -D $sn
}

x() {
  screen -h 1000000 -LAa
}

m() {
  mkdir $1
  cd $1
}

cc() {
  sudo tail -n 100 /var/log/commands.log
}

g() {
  grep -rin "$*"
}

p() {
  ps aux
}

pu() {
  ps aux -u $1
}

pg() {
  ps aux | grep -rin "$*"
}

