#!/usr/bin/env python3

import os
import sendgrid
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail


def execute(api_key, to_email, from_email, subject, html_content):
    message = Mail(
        from_email=from_email,
        to_emails=to_email,
        subject=subject,
        html_content=html_content
    )

    try:
        sg = SendGridAPIClient(api_key)
        response = sg.send(message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(e.message)
