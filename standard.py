#!/usr/bin/env python3

# python main function


import argparse


def execute():
    print('do something')


def main(args):
    print('do something')

# shell block


ap = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)

ap.add_argument('mandatory', help='This is required')
ap.add_argument('--optional', default='default', type=int)

if __name__ == '__main__':
    main(ap.parse_args())

# exit(0)
