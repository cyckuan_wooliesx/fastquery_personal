#!/usr/bin/env python3


# dependent packages

from subprocess import call


# function to transfer src to dest

def execute(src, dest):
    call(["gsutil", "-m", "rsync", "-r", "-d", src, dest])
    return 1
