#!/usr/bin/env python3


# dependent packages

from subprocess import call


# function to transfer files to dest

def execute(src):
    call(["gsutil", "rm", src])
    return 0
