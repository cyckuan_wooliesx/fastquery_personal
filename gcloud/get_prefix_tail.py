#!/usr/bin/env python3


# function to get base name or last token of prefixes

def execute(prefix):
    prefix_parts = prefix.split('/')
    prefix_length = len(prefix_parts) - 2
    return prefix_parts[prefix_length]
