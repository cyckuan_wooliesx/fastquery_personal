#!/usr/bin/env python3


# dependent packages

from google.oauth2 import service_account


# get connection

def execute(credential_path):
    credentials = service_account.Credentials.from_service_account_file(
        credential_path,)
    return credentials
