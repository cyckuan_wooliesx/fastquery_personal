#!/usr/bin/env python3

"""Command-line sample that creates a one-time transfer from Amazon S3 to
Google Cloud Storage.

This sample is used on this page:

    https://cloud.google.com/storage/transfer/create-transfer

For more information, see README.md.
"""

import datetime
import json

import googleapiclient.discovery


def execute(description, project_id, credentials, access_key_id, secret_access_key, source_bucket, sink_bucket, prefix):
    """Create a one-time transfer from Amazon S3 to Google Cloud Storage."""

    # start_date = datetime.datetime.strptime(start_date, '%Y/%m/%d')
    # start_time = datetime.datetime.strptime(start_time, '%H:%M:%S')

    start_date = datetime.date.today()
    start_time = datetime.datetime.now()

    storagetransfer = googleapiclient.discovery.build('storagetransfer', 'v1', credentials=credentials)

    # Edit this template with desired parameters.
    transfer_job = {
        'description': description,
        'status': 'ENABLED',
        'projectId': project_id,
        'schedule': {
            'scheduleStartDate': {
                'day': start_date.day,
                'month': start_date.month,
                'year': start_date.year
            },
            'scheduleEndDate': {
                'day': start_date.day,
                'month': start_date.month,
                'year': start_date.year
            },
            'startTimeOfDay': {
                'hours': start_time.hour,
                'minutes': start_time.minute,
                'seconds': start_time.second
            }
        },
        'transferSpec': {
            'awsS3DataSource': {
                'bucketName': source_bucket,
                'awsAccessKey': {
                    'accessKeyId': access_key_id,
                    'secretAccessKey': secret_access_key
                }
            },
            'gcsDataSink': {
                'bucketName': sink_bucket
            },
            'objectConditions': {
                'includePrefixes': [prefix]
            }
        }
    }

    result = storagetransfer.transferJobs().create(body=transfer_job).execute()
    print('Returned transferJob: {}'.format(json.dumps(result, indent=4)))



# from google.oauth2 import service_account
# credentials = service_account.Credentials.from_service_account_file('/home/ckuan/reco_engine.json')
# execute('test','wx-bq-poc',credentials,'AKIAJMQ5IM43P63MLJNA','lInWa0GrZODsHoykyR1r4K69+hKJRr+lLpnYhElh','data-preprod-redshift-exports','wx-prod-re','bwsre/temp/data/loyalty_modeling/bwsre_po_max/2020-03-10/')
