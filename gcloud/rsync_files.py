#!/usr/bin/env python3


# dependent packages

from subprocess import call


# function to transfer files to dest

def execute(src, dest):
    call(["gsutil", "-m", "rsync", src, dest])
    return 0
