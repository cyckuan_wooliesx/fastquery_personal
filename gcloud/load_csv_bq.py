#!/usr/bin/env python3


# dependent packages

from subprocess import call


# function to load csv files into bigquery

def execute(
        config,
        bqdataset,
        bqtable,
        schemaname,
        tablename,
        datafiles,
        schemapath,
        loadmode):

    datasettable = config['projectid'] + ":" + bqdataset + "." + bqtable
    quotes = '--quote="'
    nullmarker = '--null_marker=' + config['nullcharacter']
    fielddelimiter = '--field_delimiter=' + chr(config['asciidelimiter'])
    maxbadrecords = '--max_bad_records=' + str(config['maxbadrecords'])
    erroroutput = 'err/' + schemaname + '.' + tablename + '.err'
    sourceformat = "--source_format=" + config['loadformat']

    call(["bq", "load", sourceformat, '--' + loadmode, quotes, nullmarker,
          fielddelimiter, maxbadrecords, datasettable, datafiles, schemapath])
    return 0
