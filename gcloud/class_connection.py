#!/usr/bin/env python3


from importhelp import importall
__all__ = importall.load_all_modules(__package__, __file__, globals())

import config
import util


class connection(object):

    def __init__(self, config_path, secrets_path, secrets_key):
        self.config_path = config_path
        self.read_config()
        self.secrets_path = secrets_path
        self.secrets_key = secrets_key
        self.read_secrets()
        self.get_credentials()

    def read_config(self):
        self.config = config.read_config.execute(self.config_path)
        self.project = self.config['projectid']
        return self.config

    def read_secrets(self):
        secrets = config.read_config.execute(self.secrets_path)
        self.secrets = config.decrypt_secrets.execute(self.secrets_key, secrets)
        self.credential_path = self.secrets['serviceaccount']
        return self.secrets

    def get_secrets(self):
        return self.secrets

    def get_project(self):
        return self.project

    def get_credentials(self):
        self.credentials = get_credentials.execute(self.credential_path)
        return self.credentials

    def get_config(self):
        self.config = util.get_config.execute(self.config_path)
        return self.config

    def load_csv_bq(
            self,
            bqdataset,
            bqtable,
            schemaname,
            tablename,
            datafiles,
            schemapath,
            mode):
        return load_csv_bq.execute(
            self.config,
            bqdataset,
            bqtable,
            schemaname,
            tablename,
            datafiles,
            schemapath,
            mode)

    def bq_run_sql_df(self, sql):
        return bq_run_sql_df.execute(self.project, self.credentials, sql)

    def bq_stats(self, schemaname, tablename):
        return bq_stats.execute(self, schemaname, tablename)

    def bq_delete_table(self, schematable):
        delete_bq_table.execute(schematable)

    def bq_create_table(self, schematable, schemapath):
        create_bq_table.execute(schematable, schemapath)

    def rsync(self, src, dest):
        rsync_files.execute(src, dest)
