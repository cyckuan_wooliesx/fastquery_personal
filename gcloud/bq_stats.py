
#!/usr/bin/env python3


# dependent packages

import os


# extract ddl of specified schema.table

def execute(connection, schemaname, tablename, whereclause=''):

    schemaname = schemaname.lower()
    tablename = tablename.lower()

    # generate dynamic sql

    sql_path = os.path.join(os.path.dirname(__file__), 'sql/stats.sql')
    with open(sql_path, 'r') as sql_file:
        sql = sql_file.read()

    fsql = eval('f"""' + sql + '"""')

    stats = connection.bq_run_sql_df(fsql)
    stats = stats['df']

    return stats

