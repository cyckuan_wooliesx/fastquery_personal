#!/usr/bin/env python3


# dependent packages

from google.cloud import storage


# function returning prefixes

def execute(bucket_name, prefix):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    iterator = bucket.list_blobs(delimiter="/", prefix=prefix)
    response = iterator._get_next_page_response()
    return response['prefixes'] if 'prefixes' in response else []
