#!/usr/bin/env python3


# dependent packages

from subprocess import call


# create bigquery table using json

def execute(datasettable):
    call(["bq", "rm", "-f", "-t", datasettable])
    return 1
