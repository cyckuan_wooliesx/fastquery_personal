#!/usr/bin/env python3


# dependent packages

import sys
import datetime
import pandas as pd
import pandas_gbq
from google.oauth2 import service_account
from google.cloud import bigquery

# runs bigquery sql and returns df

def execute(project, credentials, sql, maxbytes=10000000000000):

    exec_word = sql.strip().replace('\n',' ').split(' ', 1)[0]
    print(exec_word)
    exec_nonselect = exec_word in ['create','delete','insert','update','drop','grant']

    results = {}
    results['sql_message'] = ''

    results['start_time'] = datetime.datetime.now()

    try:

        if exec_nonselect:
            client = bigquery.Client(
                project=project,
                credentials=credentials,
            )
            query_job = client.query(sql)
            rows = query_job.result()
            results['df'] = pd.DataFrame(data={exec_word:[0]})
        else:
            pandas_gbq.context.project = project
            pandas_gbq.context.credentials = credentials
            results['df'] = pd.read_gbq(
                sql,
                project_id=project,
                credentials=credentials,
                dialect='standard',
                # use_bqstorage_api=True,
                progress_bar_type=None,
                configuration={
                    'query': {
                        'maximumBytesBilled': maxbytes}})

        results['sql_code'] = 'success'

    except pandas_gbq.gbq.GenericGBQException as e:
        results['sql_code'] = 'error'
        results['sql_message'] = str(e)

    except BaseException:
        results['sql_code'] = 'error'
        results['sql_message'] = str(sys.exc_info()[1])

    else:
        results['sql_code'] = 'success'

    results['end_time'] = datetime.datetime.now()
    elapsed = results['end_time'] - results['start_time']
    results['elapsed'] = elapsed.total_seconds()

    return results
