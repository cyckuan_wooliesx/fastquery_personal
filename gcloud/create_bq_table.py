#!/usr/bin/env python3


# dependent packages

from subprocess import call


# create bigquery table using json

def execute(datasettable, schemapath):
    call(["bq", "mk", "-f", "--table", datasettable, schemapath])
    return 1
