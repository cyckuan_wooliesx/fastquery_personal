#!/usr/bin/env python3

import jenkins


def execute(server_url, username, api_token, jobname, param_dict):
    jurl = 'https://%s:%s@%s' % (username, api_token, server_url)
    jserver = jenkins.Jenkins(jurl)
    jobs = jserver.get_jobs()
    return jobs
