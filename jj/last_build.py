#!/usr/bin/env python3

import jenkins


def execute(server_url, username, api_token, jobname, param_dict):
    jurl = 'https://%s:%s@%s' % (username, api_token, server_url)
    jserver = jenkins.Jenkins(jurl)
    last_build_number = jserver.get_job_info(
        jobname)['lastCompletedBuild']['number']
    build_info = jserver.get_build_info(jobname, last_build_number)
    return build_info
