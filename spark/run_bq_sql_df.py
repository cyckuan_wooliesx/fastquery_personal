#!/usr/bin/env python3


# dependent packages

import sys
import datetime
import pandas as pd
import pandas_gbq
from google.oauth2 import service_account


# runs bigquery sql and returns df

def execute(project, credentials, sql, maxbytes=10000000000000):

    pandas_gbq.context.project = project
    pandas_gbq.context.credentials = credentials

    results = {}
    results['sql_message'] = ''

    results['start_time'] = datetime.datetime.now()

    try:
        results['df'] = pd.read_gbq(
            sql,
            project_id=project,
            credentials=credentials,
            dialect='standard',
            configuration={
                'query': {
                    'maximumBytesBilled': maxbytes}})

        results['sql_code'] = 'success'

    except pandas_gbq.gbq.GenericGBQException as e:
        results['sql_code'] = 'error'
        results['sql_message'] = str(e)

    except BaseException:
        results['sql_code'] = 'error'
        results['sql_message'] = str(sys.exc_info()[1])

    else:
        results['sql_code'] = 'success'

    results['end_time'] = datetime.datetime.now()
    elapsed = results['end_time'] - results['start_time']
    results['elapsed'] = elapsed.total_seconds()

    return results
