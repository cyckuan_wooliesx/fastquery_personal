#!/usr/bin/env python3

import os
import yaml


def execute(yaml_config, file_config, dry=1, override_params=dict()):

    with open(yaml_config) as cfgf:
        t = yaml.load(cfgf, Loader=yaml.FullLoader)
        t.update(override_params)

    for c in file_config:
        with open(c[1]) as cfgf:
            txt = cfgf.read()
            txt = [t.strip() for t in txt.split('\n') if t.strip() != '']

            t[c[0]] = c[2].join(txt)
            # print(c[0] + '\n' + t[c[0]] + '\n')


    cmd = eval('f"""' + t['cmd'] + '"""')
    print(cmd + '\n')

    if dry == 0:
        os.system(cmd)

    return(cmd)
