#!/usr/bin/env python3



import os
from cleancode import remove_sql_comments
from gcloud import transfer_files



def execute(pipe_path,bucket):

    pipe_dir = os.path.dirname(pipe_path)

    with open(pipe_path) as f:
        pipe_list = remove_sql_comments.execute(f.read()).splitlines()

    pipe_list = [p for p in pipe_list if p.strip() != '']

    sql_list = []
    for i,s in enumerate(pipe_list):

        # transfer file

        src = os.path.join(pipe_dir,s)
        dest = os.path.join(bucket,s)
        transfer_files.execute(src,dest)

        # add workflow template entry

        sql_list.append('- stepId: sql' + str(i))

        if i > 0:
            sql_list.append('  prerequisiteStepIds: [ sql' + str(i-1) + ' ]')

        sql_list.append('  sparkSqlJob:')
        sql_list.append('    queryFileUri: ' + dest)

    print('\n'.join(sql_list))




# pipe_path = '/home/ckuan/fastquery_scaffold/sql_prod/pipe_config_supre.sql'
# bucket = 'gs://wx-prod-re/fastquery'
# execute(pipe_path, bucket)




