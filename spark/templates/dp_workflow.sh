gcloud dataproc workflow-templates set-managed-cluster template-id \
    --master-machine-type machine-type \
    --worker-machine-type machine-type \
    --num-workers number \
    --cluster-name cluster-name
