#!/usr/bin/env python3


import os
from . import cluster_cmd


def execute(yaml_config, dry=1):

    cwd = os.path.dirname(__file__)

    file_config = [
        ['cmd', os.path.join(cwd,'templates/dp_ssh_tunnel.sh'), ' ']
    ]

    cluster_cmd.execute(yaml_config, file_config, dry)
