#!/usr/bin/env python3


from . import cluster_cmd
import os


def execute(yaml_config, properties, metadata, initialization, dry=1):

    cwd = os.path.dirname(__file__)

    file_config = [
        ['cmd', os.path.join(cwd,'templates/dp_create_template.sh'), ' ']
        , ['properties', properties, ',']
        , ['metadata', metadata, ',']
        , ['initialization', initialization, ',']
    ]

    cluster_cmd.execute(yaml_config, file_config, dry)
