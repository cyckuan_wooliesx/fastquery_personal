#!/usr/bin/env python3

import os
import util
import livequery as lq

from IPython.display import display, HTML


def execute(sql_watch, config_path, config_key, show=0):

    # config
    config = util.get_config(config_path)

    # db
    db_path = config['dbconfig'][config_key]

    # watch path
    watch_path = os.path.dirname(sql_watch)

    # alias path
    alias_path = config['alias']

    # results
    results_path = os.path.join(watch_path, config['results']['template'])

    # presql
    global_presql_path = config['presql']['main']
    presql_path = os.path.join(watch_path, config['presql']['template'])

    # width
    width = config['displaywidth']

    # exec modes
    r = lq.run(
        db_path,
        alias_path,
        results_path,
        global_presql_path,
        presql_path,
        width)
    results = r.start(sql_watch)
    if show == 1:
        display(results[0])

    return results
