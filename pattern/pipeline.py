#!/usr/bin/env python3


# dependents

import os
from importhelp import importall



def execute(cargo,prefix,codepath):

    if os.path.isfile(codepath):
        codepath = os.path.dirname(codepath)

    proclist = os.listdir(codepath)

    proclist = sorted([os.path.splitext(p)[0]
                       for p in proclist
                       if p.endswith('.py') and p.split('_')[0] == prefix
                      ])


    mods = dict()
    importall.load_all_modules(__package__, codepath, mods)

    for proc in proclist:
        module = mods[proc]
        cargo = module.execute(cargo)
        if 'stop' in cargo:
            break

    return cargo
