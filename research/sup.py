#!/usr/bin/env python3

import os
from spark import create_cluster
from spark import ssh_tunnel


cfg = '/home/ckuan/fastquery_scaffold/config/spark'


create_cluster.execute(
os.path.join(cfg,'spark_config.yaml')
, os.path.join(cfg,'properties.txt')
, os.path.join(cfg,'metadata.txt')
, os.path.join(cfg,'initialization.txt')
, dry=0
)


ssh_tunnel.execute(
os.path.join(cfg,'spark_config.yaml')
, dry=0
)



