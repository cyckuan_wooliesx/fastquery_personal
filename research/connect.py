#!/usr/bin/env python3

import pandas as pd
from pyhive import hive

host_name = "127.0.0.1"
port = 10000
user = "ckuan"
password = ""
database="supre"



def hiveconnection(host_name, port, user, database):
    conn = hive.Connection(host=host_name, port=port, username=user, database=database)
    # cur = conn.cursor()
    # cur.execute('select count(distinct subcat_name) from supre.context_ipi_cat')
    # result = cur.fetchall()

    sql = "select count(1) as c from supre.context_ipi_cat"
    df = pd.read_sql(sql, conn)

    return df



# Call above function
output = hiveconnection(host_name, port, user, database)
print(output)
