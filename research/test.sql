

-- Agreed pattern

-- CREATE DATABASE supre;
USE supre;


drop table if exists
art_master
;

CREATE table art_master
(prod_nbr string
, division_nbr smallint
, article_nbr string
, article_name string
, division_name string
, scan_desc string
, whse_desc string
, online_desc string
, master_desc string
, uom string
, uom_ctgry smallint
, unit_of_measure_category_desc string
, pack_size smallint
, pack_size_uom string
, height decimal(12,10)
, height_uom string
, width decimal(12,10)
, width_uom string
, length decimal(12,10)
, length_uom string
, volume decimal(15,10)
, volume_uom string
, gross_weight decimal(12,10)
, gross_weight_uom string
, net_weight decimal(12,10)
, net_weight_uom string
, all_dept_code string
, all_dept_name string
, dept_code string
, dept_name string
, dept_mgr_id string
, dept_mgr_name string
, ctgry_code string
, ctgry_name string
, ctgry_mgr_id string
, ctgry_mgr_name string
, subcat_code string
, subcat_name string
, subcat_mgr_id string
, subcat_mgr_name string
, sgmnt_code string
, sgmnt_name string
, sgmnt_mgr_id string
, sgmnt_mgr_name string
, buying_ctgry_mgr_username string
, buying_ctgry_mgr_postn string
, head_of_trade_username string
, head_of_trade_postn string
, general_mgr_username string
, general_mgr_postn string
, price_family_code string
, price_family_name string
, vendor_brand string
, brand_label string
, brand_code_desc string
, quantium_sub_brand string
, quantium_brand string
, quantium_super_brand string
, article_type string
, article_type_desc string
, master_size decimal(9,4)
, master_size_uom string
, comparative_size_au integer
, comparative_size_au_uom string
, comparative_size_nz integer
, comparative_size_nz_uom string
, country_id string
, tax_category string
, tax_classification string
, tax_rate string
, tax_code string
, pack_breakdown_ind string
, pack_breakdown_ordering_uom string
, prod_id string
, srvng_size decimal(10,3)
, srvng_size_uom string
, nbr_of_serves decimal(10,4)
, nutritional_claim string
, mandatory_warning string
, storage_req string
, tot_shelflife_in_days smallint
, min_shelflife_in_days smallint
, mrcds_ctgry_lvl_1_code int
, mrcds_ctgry_lvl_1_name string
, mrcds_ctgry_lvl_2_code int
, mrcds_ctgry_lvl_2_name string
, mrcds_ctgry_lvl_3_code int
, mrcds_ctgry_lvl_3_name string
, mrcds_ctgry_lvl_4_code int
, mrcds_ctgry_lvl_4_name string
, base_mrcds_ctgry_code string
, base_mrcds_ctgry_name string
, row_status_ind string
, last_update_date string
, insert_date string
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' LOCATION 'gs://wx-prod-re/temp/art_master/article_master/';

drop table if exists
supre.article_master
;

create table supre.article_master
using PARQUET
AS
SELECT * from art_master
;


select
count(1) as c
from
art_master
;


--select prod_nbr, article_nbr from supre.article_master limit 10;


-- OPTIONS (
--  PATH "gs://cmd-incoming-20190220/cmd_output/bi_article_sales_petrol",
--  CODEC "gzip",
--  delimiter "|",
--  header "false",
--  inferSchema "false",
--  nullValue "<N>"
-- );
