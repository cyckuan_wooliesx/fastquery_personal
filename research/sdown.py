#!/usr/bin/env python3

import os
from spark import delete_cluster


cfg = '/home/ckuan/fastquery_scaffold/config/spark'


delete_cluster.execute(
    os.path.join(cfg,'spark_config.yaml')
    , dry=0
)


