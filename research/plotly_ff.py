#!/usr/bin/env python3

import os
import plotly
import plotly.plotly as py
import plotly.figure_factory as ff
import numpy as np

import json
from flask import Flask, render_template


def execute(port):

    t = np.linspace(-1, 1.2, 2000)
    x = (t**3) + (0.3 * np.random.randn(2000))
    y = (t**6) + (0.3 * np.random.randn(2000))

    colorscale = ['#7A4579', '#D56073',
                  'rgb(236,158,105)', (1, 1, 0.2), (0.98, 0.98, 0.98)]

    fig = ff.create_2d_density(
        x, y, colorscale=colorscale,
        hist_color='rgb(255, 237, 222)', point_size=3
    )

    graphs = [
        dict(
            data=fig, layout={'title': 'test'}
        )
    ]

    ids = ['graph-{}'.format(i) for i, _ in enumerate(graphs)]

    graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)

    app = Flask(
        __name__,
        template_folder=os.path.join(
            os.path.dirname(__file__),
            'templates'))

    @app.route('/')
    def index():
        return render_template('index.html', ids=ids, graphJSON=graphJSON)

    try:
        app.run(port=str(port))
    except BaseException:
        pass


execute(19998)
