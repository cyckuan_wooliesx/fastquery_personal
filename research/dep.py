#!/usr/bin/env python3


# break_tag = '--<break>'


# dependents

import argparse
import re
import os
import string
import util

from livequery import sanitize_sql


# reserved words

with open('redshift_reserved.txt', 'r') as rkw:
    redshift_keywords = rkw.read().splitlines()

dep_target_patterns = [[['CREATE', 'TABLE'], 'create'], [['CREATE', 'TEMP', 'TABLE'], 'createtemp'], [
    ['INSERT', 'INTO'], 'insert'], [['UPDATE'], 'update'], [['DELETE'], 'delete']]

dep_sub_pattern = ['(', 'SELECT']

dep_source_patterns = ['FROM', 'JOIN']

facets = ['select', 'from', 'where', 'groupby', 'having']


# support functions

def strip_all_punctuation(t):
    return t.strip().translate(str.maketrans('', '', string.punctuation))


def strip_end_punctuation(t):
    return t.strip().strip('\"').strip("\'").translate(
        str.maketrans('', '', '(){}<>;'))


# main


def execute(sql_dir, ml, msql, params):

    sqlfile = ml[1]

    # title

    util.figlet_title('univers', 'sql parse')

    # sql file

    orig_folder = os.path.dirname(os.path.abspath(sqlfile))
    orig_name_ext = os.path.basename(sqlfile)
    orig_name, orig_ext = os.path.splitext(orig_name_ext)

    with open(sqlfile, 'r') as sqlf:
        fulltext = sqlf.read()
        parsed_sql = sanitize_sql(
            sqlfile, fulltext, aliases=params['aliases'])['sql']

    # initialize statement loop

    dep_sets = []

    for ps in parsed_sql:

        # manage brackets

        ps = re.sub(r'(?!(([^"]*"){2})*[^"]*$)\(', '<bracketopen>', ps)
        ps = re.sub(r"(?!(([^']*'){2})*[^']*$)\(", "<bracketopen>", ps)
        ps = re.sub(r'(?!(([^"]*"){2})*[^"]*$)\)', '<bracketclose>', ps)
        ps = re.sub(r"(?!(([^']*'){2})*[^']*$)\)", "<bracketclose>", ps)

        ps = re.sub(r'\(', ' ( ', ps)
        ps = re.sub(r'\)', ' ) ', ps)

        # terms

        t = re.split(r'\n|\t|\s', str(ps))
        t = [i for i in t if i != '']

        # original terms

        tc = t

        # uppercase terms

        tu = [strip_all_punctuation(tt.upper()) for tt in t]

        # initialize word loop

        target_found = 0

        sources = []
        source_aliases = []

        nest_sub_deepest = 0
        nest_sub = 0
        nest_expr = 0

        target = ''
        tclass = ''

        n = len(t) - 3
        i = 0

        while i < n:

            if tu[i] != '':

                # find target

                if target_found == 0:
                    for p in dep_target_patterns:
                        lp = len(p[0])
                        if tu[i:i + lp] == p[0]:

                            target_found = 1
                            tclass = p[1]
                            tidx = i + lp

                            tcv = tc[tidx]
                            tcv = strip_end_punctuation(tcv)

                            target = tcv
                            print("\ntarget: " + target)

                            facet_current = 'select'

                # track facet

                if tu[i] == 'FROM':
                    facet_current = 'from'

                if tu[i] == 'WHERE':
                    facet_current = 'where'

                if tu[i] == 'GROUP':
                    facet_current = 'groupby'

                if tu[i] == 'HAVING':
                    facet_current = 'having'

                # track nesting

                if tu[i:i + 2] == dep_sub_pattern:
                    nest_sub += 1
                elif tu[i] == '(':
                    nest_expr -= 1

                if tu[i] == ')' and nest_expr == 0:
                    nest_sub += 1
                else:
                    nest_expr -= 1

                if nest_sub > nest_sub_deepest:
                    nest_sub_deepest = nest_sub

                # find sources

                if target_found > 0:

                    for p in dep_source_patterns:

                        if tu[i] == p:
                            tcv = strip_end_punctuation(tc[i + 1])

                            if tu[i + 1] != '':
                                sources.append(tcv)
                                print("  source: " + tcv)

                                # find alias

                                if tu[i + 2] == 'AS':
                                    alias = strip_end_punctuation(tc[i + 3])
                                    if tu[i + 3] in redshift_keywords:
                                        alias = ''
                                elif tu[i + 2] in redshift_keywords:
                                    alias = ''
                                else:
                                    alias = strip_end_punctuation(tc[i + 2])

                                source_aliases.append(alias)

            # loop increment, next word

            i += 1

        # collect results

        if target_found > 0:
            dep_sets.append([target, tclass, tidx, sources,
                             source_aliases, nest_sub_deepest])

    # print tables

    for ds in dep_sets:
        print('\n' + str(ds))

    # return dep_sets
    return msql
