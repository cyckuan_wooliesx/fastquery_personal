#!/usr/bin/env python3


import recurse


ROOTPATH = '/home/ckuan/lib/fastquery'
FILEPATTERN = r".*\.py$"

PARAM_DICT = {'DRY': 1}

results = recurse.loop.execute(
    ROOTPATH,
    FILEPATTERN,
    recurse.staticfn.execute,
    PARAM_DICT)

# for r in results:
#    if r[1] == 0:
#        print(r)
