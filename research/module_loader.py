#!/usr/bin/env python3

import initloader
__all__ = initloader.load_all_modules(__package__, __file__)

print('\n\n*** name ***\n%s' % __name__)
print('\n\n*** package ***\n%s' % __package__)
print('\n\n*** loader ***\n%s' % __loader__)
print('\n\n*** spec ***\n%s' % __spec__)
print('\n\n*** all ***\n%s' % __all__)
print('\n\n*** dir ***\n%s' % dir())
print('\n\n*** globals ***\n%s' % globals())
print('\n\n*** locals ***\n%s' % locals())
print('\n\n*** vars ***\n%s' % vars())
