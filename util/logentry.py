#!/usr/bin/env python3


# dependents

import datetime


# functions

def execute(log_file, message):
    with open(log_file, 'a+') as lf:
        lf.write(str(datetime.datetime.now()) + '\n')
        lf.write(message + '\n')

    return 0
