#!/usr/bin/env python3


# dependent packages

from subprocess import call


# extracts and merge pbzip2 files

def execute(cpu, mem, src, dest):
    call(["pbzip2", "-cdk", "-r", "-p" + cpu, "-m" + mem, src, dest])
    return 0
