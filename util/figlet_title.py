#!/usr/bin/env python3


# dependent packages

from pyfiglet import Figlet
from sty import fg, bg, ef, rs

# function to transfer src to dest

def execute(font, text, colour='li_white'):

    f = Figlet(font=font)
    print(fg(colour) + f.renderText(text) + fg.rs)

    return 0
