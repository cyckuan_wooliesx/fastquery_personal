#!/usr/bin/env python3


# dependent packages

from subprocess import call


# function to transfer src to dest

def execute(cmd, param):
    call(["stty", cmd, param])
    return 0
