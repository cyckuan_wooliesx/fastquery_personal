#!/usr/bin/env python3

import datetime
import pytz


def execute(timestring, tz='Australia/Sydney'):
    local = pytz.timezone(tz)
    naive = datetime.datetime.strptime(timestring, "%Y-%m-%d %H:%M:%S")
    local_dt = local.localize(naive, is_dst=None)
    utc_dt = local_dt.astimezone(pytz.utc)
    return utc_dt
