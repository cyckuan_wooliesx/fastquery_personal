#!/usr/bin/env python3

import os


def execute(filename):

    if os.path.exists(filename):
        with open(filename) as f:
            c = f.read().strip()
            if c.isnumeric():
                c = int(c)
            else:
                c = 0
    else:
        c = 0

    return c
