#!/usr/bin/env python3


# function to execute nested lists

def execute(l):
    if l == []:
        return l
    if isinstance(l[0], list):
        return execute(l[0]) + execute(l[1:])
    return l[:1] + execute(l[1:])
