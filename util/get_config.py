#!/usr/bin/env python3


# dependent packages

import yaml


# function returning config specified in yaml file

def execute(config_path):
    with open(config_path, 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    return config
