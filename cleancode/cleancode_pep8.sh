#!/usr/bin/env bash

import os
d=os.path.join('/home',os.environ['USER'],'LIB','fastquery')

for py in `find $d -name "*.py"`; do
  echo $py
  autopep8 --in-place --aggressive --aggressive $py
done


