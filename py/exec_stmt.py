#!/usr/bin/env python3

import datetime
import sys

def execute(meta_sql, stmt):
    results = {}
    results['df'] = None
    results['sql_message'] = ''

    results['start_time'] = datetime.datetime.now()

    try:
        stmt = stmt.strip()
        if stmt[-1] == ';':
            stmt = stmt[:-1]
        exec(stmt)
        results['sql_code'] = 'success'
    except:
        meta = '%error ' + str(sys.exc_info()[1])
        results['sql_code'] = 'error'

    results['end_time'] = datetime.datetime.now()
    elapsed = results['end_time'] - results['start_time']
    results['elapsed'] = elapsed.total_seconds()

    return results, meta_sql
