import os
import importlib.util
from importhelp import import_globals
from . import importlist_by_file


def load_all_modules(package, fileref, global_dict={}):

    pyfile_ext = ['.py']
    py_dir = os.path.dirname(fileref)
    py_files = os.listdir(py_dir)
    py_files = [f for f in py_files if f != os.path.basename(fileref)]

    filelist = [os.path.join(py_dir, f) for f in py_files if os.path.splitext(f)[
        1] in pyfile_ext and not f.startswith('__')]

    all = importlist_by_file.load_many_modules(package, filelist)

    global_dict.update(import_globals.mods_dict)

    return all
