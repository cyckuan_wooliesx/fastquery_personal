import os
import importlib


def load_all_mods(modulename, global_dict={}):

    mod = __import__(modulename)
    all = [d for d in dir(mod) if d[:2] != '__']

    for m in all:
        if hasattr(m, "execute"):
            global_dict[m] = getattr(m, "execute")

    return all
