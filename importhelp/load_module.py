#!/usr/bin/env python3

import importlib.machinery
import importlib.util
import os


def execute(filepath):

    modname = os.path.basename(filepath)
    loader = importlib.machinery.SourceFileLoader(modname, filepath)
    spec = importlib.util.spec_from_loader(loader.name, loader)
    mod = importlib.util.module_from_spec(spec)
    loader.exec_module(mod)

    return mod
