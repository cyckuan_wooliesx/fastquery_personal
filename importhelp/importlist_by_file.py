

from importhelp import importone_by_file


def load_many_modules(package, filelist):

    all = []
    for fileref in filelist:
        a = importone_by_file.load_one_module(package, fileref)

        all.extend(a)

    return all
