#!/usr/bin/env python3


import types


SPECIAL_CLASSES = ['class']

mods_dict = dict()
functions_dict = dict()
files_set = set()


def find_in_globals(modname, dict):
    if modname in dict:
        print(dict[modname])

def print_all_mods():
    for m in mods_dict:
        print(m)

def print_all_functions():
    global mods_dict, functions_dict
    for fn in functions_dict:
        print(fn)

def update(global_dict):
    global mods_dict, functions_dict
    global_dict.update(mods_dict)

def update_all():
    global mods_dict, functions_dict

    for m in mods_dict:
        if isinstance(m, types.ModuleType):
            m.globals().update(mods_dict)
            # m.globals().update(functions_dict)

