#!/usr/bin/env python3

import os
import types
import importlib.util
from importhelp import load_module
from importhelp import import_globals


SPECIAL_CLASSES = ['class']


def load_one_module(package, fileref):

    modname = os.path.splitext(os.path.basename(fileref))[0]

    if fileref in import_globals.files_set or modname in import_globals.mods_dict:
        pass
    elif modname.split('_')[0] not in import_globals.SPECIAL_CLASSES:

        # mod = load_module.execute(fileref)
        mod = importlib.import_module('.%s' % modname, package)

        import_globals.files_set.add(fileref)
        import_globals.mods_dict[modname] = mod

        # if hasattr(mod, "execute"):
        #    print('execute fn ' + modname)
        #    import_globals.functions_dict[modname] = getattr(mod, "execute")

    return [modname]
