from importhelp import importone


def load_modules(package, filelist, global_dict={}):

    # if isinstance(modlist, str):
    #    modlist = [m for m in modlist.split() if m != '']

    all = []
    for fileref in filelist:
        a, gd = importone.load_one_module(
            package,
            fileref,
            global_dict)
        all.extend(a)
        global_dict.update(gd)

    return all, global_dict
