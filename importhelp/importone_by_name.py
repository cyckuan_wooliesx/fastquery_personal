#!/usr/bin/env python3

import os
import types
import importlib.util
from importhelp import import_globals


def load_one_module(package, modname):

    if modname.split('_')[0] not in import_globals.SPECIAL_CLASSES:

        mod = importlib.import_module('.%s' % modname, package)
        import_globals.mods_dict[modname] = mod

        if hasattr(mod, "execute"):
            import_globals.functions_dict[modname] = getattr(mod, "execute")

    return [modname],
