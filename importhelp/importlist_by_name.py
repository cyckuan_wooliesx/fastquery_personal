from importhelp import importone_by_name


def load_modules(package, modlist, global_dict={}):

    if isinstance(modlist, str):
        modlist = [m for m in modlist.split() if m != '']

    all = []
    for modname in filelist:
        a, gd = importone_by_name.load_one_module(
            package,
            fileref,
            global_dict)
        all.extend(a)
        global_dict.update(gd)

    return all, global_dict
