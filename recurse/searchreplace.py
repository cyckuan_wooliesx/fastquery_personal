#!/usr/bin/env python3

import re


def execute(file, params):

    with open(file) as f:
        text = f.read()
        text = text.split('\n')

    new_text = []
    found = 0
    for t in text:
        if re.search(params['SEARCHSTRING'], t):
            new_t = re.sub(params['SEARCHSTRING'], params['REPLACESTRING'], t)
            if params['REMOVE_LAST'] > 0:
                new_t = new_t[:-1]
            new_text.append(new_t)
            found += 1
        else:
            new_text.append(t)

    if found > 0:
        new_text = '\n'.join(new_text)
        print(new_text)
        if params['DRY'] == 0:
            with open(file, "w") as wf:
                wf.write(new_text)

    stats = [file, found]
    return stats
