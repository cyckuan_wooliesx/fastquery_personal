#!/usr/bin/env python3


import os
import re


def execute(rootpath, patternstring, process, params):

    pattern = re.compile(patternstring)

    results = []

    for r, d, f in os.walk(rootpath):

        for file in f:
            if pattern.match(
                    file) is not None and file != os.path.basename(__file__):
                rf = os.path.join(r, file)
                results.append(process(rf, params))

        for folder in d:
            rd = os.path.join(r, folder)
            results.extend(execute(rd, patternstring, process, params))

    return results
