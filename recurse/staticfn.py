#!/usr/bin/env python3

import re


def execute(file, params):

    with open(file) as f:
        text = f.read()
        text = text.split('\n')

    new_text = []
    found = 0

    ln_header = -1
    ln_declare_min = -1
    ln_import_min = -1
    ln_import_max = -1
    ln_function = -1


    # scan

    for ln, t in enumerate(text):

        tl = t.lower().strip()

        if tl.startswith('#!/usr/bin/env'):
            ln_header = ln

        if tl.find('=') >= 0:
            if ln_declare_min < 0:
                ln_declare_min = ln

        if tl.find('import') >= 0:
            if ln_import_min < 0:
                ln_import_min = ln
            if ln_import_max < ln:
                ln_import_max = ln

        if ln_function < 0 and (tl.find('class ') >= 0 or tl.find('def ') >= 0):
            ln_function = ln
            break

    if ln_function < 0:
        ln_function = len(text)


    # gather

    t_header = []
    t_import = []
    t_declare = []
    t_function = []

    if ln_header >= 0:
        t_header.append(text[ln_header])

    for ln, t in enumerate(text):

        tl = t.lower().strip()

        if ln < ln_function:

            if tl.find('import') >= 0:
                t_import.append(t)

            if tl.find('=') >= 0:
                t_declare.append(t)

        else:
            break

    t_function = text[ln_function:]


    # assemble

    new_text = []

    if len(t_header) > 0:
        new_text.extend(t_header)

    if len(t_import) > 0:
        new_text.extend(['# imports',''] + t_import)

    if len(t_declare) > 0:
        new_text.extend(['','# constants',''] + t_declare)

    if len(t_function) > 0:
        new_text.extend(['','# functions',''] + t_function)



    print(file)
    # print('\n'.join(new_text))


    # exceptions

    for ln, t in enumerate(text):

        if t not in new_text:
          print(t)


        # new_text = '\n'.join(new_text)
        # print(new_text)
        # if params['DRY'] == 0:
        #    with open(file, "w") as wf:
        #        wf.write(new_text)

    stats = [file, 0]

    return stats
