#!/usr/bin/env python3

import os
import plotly
import json
from flask import Flask, render_template


def execute(graphs, port):

    ids = ['graph-{}'.format(i) for i, _ in enumerate(graphs)]
    graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)

    app = Flask(
        __name__,
        template_folder=os.path.join(
            os.path.dirname(__file__),
            'templates'))

    @app.route('/')
    def index():
        return render_template('index.html', ids=ids, graphJSON=graphJSON)

    try:
        app.run(port=str(port))
    except BaseException:
        pass
