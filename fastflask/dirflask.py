#!/usr/bin/env python3

import os.path
from flask import Flask
from flask_autoindex import AutoIndex


def execute(path, port):
    app = Flask(__name__)
    AutoIndex(app, browse_root=path)

    try:
        app.run(port=str(port))
    except BaseException:
        print('Keypress exit ...')
