#!/usr/bin/env python3


# dependent packages

import yaml


# get connection

def execute(config_path):
    with open(config_path, 'r') as db_config_file:
        dbc = yaml.load(db_config_file, Loader=yaml.FullLoader)
        return dbc
