#!/usr/bin/env python3


import crypt


def execute(secrets_key, secrets_dict):

    with open(secrets_key) as f:
        key = f.read().encode()

    for k in secrets_dict:
        s = secrets_dict[k]
        secrets_dict[k] = crypt.decrypt_message.execute(s, key)

    return secrets_dict
