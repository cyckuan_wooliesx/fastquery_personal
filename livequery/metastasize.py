#!/usr/bin/env python3


# dependent packages

import re
import os
import sys
import importlib.util
from importhelp import load_module
from . import select_lang


LANG_LIST = ['rs','gbq','py','spark','mmm']


# constants

META_LIB_PATH = os.path.dirname(__file__) + '/meta/meta_'
META_CHARS = ['%', '$']

RUNTIME_META = ['df']



def execute(meta_sql):

    sql_dir = meta_sql['sql_path']
    aliases = meta_sql['aliases']
    statement = meta_sql['statement']
    meta_sql['lang'] = meta_sql['dbms']


    # init

    meta_list = []


    # line by line processing

    fsql = []
    lastl = ''
    end_flag = 0

    for l in statement.split('\n'):

        l = l.strip()

        # process long lines minimum 2 char for meta

        if len(l) > 2:

            meta_found = 0

            # meta tags

            if l[0] in META_CHARS and l[1] not in META_CHARS:
                meta_params = l[1:]
                meta_found = 1

            # meta found processing - single line
            # future to include multi-line

            if meta_found > 0:

                ml = meta_params.split(' ')
                ml = [m for m in ml if m.strip() != '']
                meta_cmd = ml[0]
                meta_line = meta_params

                if meta_cmd in LANG_LIST:
                    meta_sql['lang'] = meta_cmd
                    print(meta_cmd)

                # add to global meta pool

                meta_list.append(meta_line)

                # start of statement

                meta_modulefile = META_LIB_PATH + meta_cmd + '.py'

                if os.path.exists(meta_modulefile):

                    # try:

                    mm = load_module.execute(meta_modulefile)
                    fsql = mm.execute(sql_dir, ml, fsql, meta_sql)

                    if len(fsql) > 1:
                        sl = fsql[-1].split(' ')
                        if sl[0] == '%end':
                            lastl = ' '.join(sl[1:])
                            del fsql[-1]

                    # except:
                    #  meta = '%error ' + str(sys.exc_info()[1])
                    #  global_meta[str(statement_id)].append(meta)

            # meta not found

            else:
                fsql.append(l)

        # process short lines no meta

        else:
            fsql.append(l)

    # end of statement

    if lastl != '':
        fsql.append(lastl)

    # fsql

    fsql = [f for f in fsql if len(f) > 0]
    fsql = '\n'.join(fsql)
    fsql = fsql.strip()

    return meta_list, fsql
