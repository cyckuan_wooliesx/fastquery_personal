#!/usr/bin/env python3


# dependents

import os

from importhelp import importall
__all__ = importall.load_all_modules(__package__, __file__, globals())

from . import processors


# defaults

DEFAULT_WIDTH = 150


# run

class watchrunner():

    def __init__(
            self,
            dbms,
            domain,
            config_path,
            secrets_path,
            secrets_key,
            sql_path,
            sql,
            aliases,
            results_path,
            presql,
            width=DEFAULT_WIDTH):

        self.dbms = dbms

        self.domain = domain
        self.config_path = config_path
        self.secrets_path = secrets_path
        self.secrets_key = secrets_key

        self.sql_path = sql_path
        self.sql = sql
        self.aliases = aliases

        self.results_path = results_path

        self.presql = presql
        self.width = width

    def start(self, sql):
        self.sql_path = sql
        self.sql = sql
        if os.path.isfile(sql):
            ensure_path.execute(os.path.join(os.path.dirname(sql), self.results_path))

        result = processors.main.execute(self)

        return result
