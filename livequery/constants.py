#!/usr/bin/env python3


SELECT_WORDS = {'select'}
NON_SELECT_WORDS = {
    'insert',
    'create',
    'truncate',
    'drop',
    'delete',
    'alter',
    'set',
    'grant',
    'copy',
    'update',
    'commit',
    'rollback'}

LIMIT = {'limit'}


