#!/usr/bin/env python3


# dependent packages

import re
import os
import sys
from importhelp import load_module


# constants

def execute(libname, exec_flag, meta_sql):

    META_LIB_PATH = os.path.dirname(__file__) + '/' + libname + '/'

    # print(meta_sql['meta_current'])

    for cmd in meta_sql['meta_current']:

        meta_cmd_list = cmd.split(' ')
        meta_cmd = meta_cmd_list[0]

        meta_cmd_flag = meta_cmd[0]

        if meta_cmd_flag == exec_flag:
            meta_cmd = meta_cmd[1:]
            meta_params = meta_cmd_list[1:]
            meta_modulefile = META_LIB_PATH + meta_cmd + '.py'

            if os.path.exists(meta_modulefile):

                try:
                    mod = load_module.execute(meta_modulefile)
                    fsql = mod.execute(meta_params, meta_sql)
                except:
                    meta = '%error ' + str(sys.exc_info()[1])
                    print(meta)

    return meta_sql

