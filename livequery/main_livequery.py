#!/usr/bin/env python3

import click
import os

import livequery as lq
import util


# config

@click.command()
@click.option('-d', 'domain', help='domain to run in', required=True)
@click.option('-s', 'sql_watch', help='sql file to run or folder to watch', required=True)
@click.option('-c', 'config_path', help='config file', required=True)
@click.option('-k', 'config_key', help='config key', default='main', required=False)
@click.option('-q', 'dbms', help='database system', default='rs', required=False)
@click.option('-m', 'mode', help='operational mode', default='run', required=False)
@click.option('-o', 'results', help='output location for results', default='results', required=False)

def main(domain, sql_watch, config_path, config_key, dbms, mode, results):

  # title
  util.figlet_title.execute('univers', dbms, 'li_blue')
  util.figlet_title.execute('univers', domain)

  # config
  config = util.get_config.execute(config_path)

  # db
  db_path = config['dbconfig'][config_key]
  secrets_path = config['secrets']
  secrets_key = config['secrets_key']

  # operational mode

  if os.path.isfile(sql_watch):
    op_mode = mode
    watch_path = os.path.dirname(sql_watch)
  else:
    op_mode = 'watch_' + mode
    watch_path = '/'.join([config['watch'],sql_watch])
    if not os.path.exists(watch_path):
      watch_path = '/'.join([config['watch'],'sql_' + sql_watch])
      if not os.path.exists(watch_path):
        print('ABORT : path not found')
        exit(-1)
    # if not os.path.exists(watch_path):
    #  op_mode = mode
    #  watch_path = config['watch']

  # alias path
  alias_path = config['alias']

  # results
  if os.path.isdir(results):
    results_path = results
  else:
    results_path = watch_path + '/' + config['results']['template']

  # presql
  global_presql_path = config['presql']['main']
  presql_path = watch_path + '/' + config['presql']['template']

  # width
  width = config['displaywidth']
  util.stty.execute('columns',str(width))

  # exec modes
  if op_mode in ['run','translate']:

    # runner instance
    r = lq.class_runner.runner(op_mode, dbms, domain, db_path, secrets_path, secrets_key, alias_path, results_path, global_presql_path, presql_path, width)
    return r.start(sql_watch)

  else:

    # watcher instance
    w = lq.class_watcher.watcher(op_mode, dbms, domain, db_path, secrets_path, secrets_key, watch_path, alias_path, results_path, global_presql_path, presql_path, config['events']['trigger'], config['events']['prime'], width)
    w.start()


if __name__ == '__main__':
    main()

