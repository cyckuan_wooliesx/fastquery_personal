#!/usr/bin/env python3


# dependents

import os
import pandas as pd
from sty import fg, bg, ef, rs


# functions

def execute(meta_sql):

    print(fg.green + 'Done.' + fg.rs + '\n\n')
