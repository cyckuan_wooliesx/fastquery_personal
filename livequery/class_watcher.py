#!/usr/bin/env python3


# dependents

import os

from importhelp import importall
__all__ = importall.load_all_modules(__package__, __file__, globals())

from . import processors


# defaults

DEFAULT_WIDTH = 150


# watch

class watcher():

    def __init__(
            self,
            op_mode,
            dbms,
            domain,
            config_path,
            secrets_path,
            secrets_key,
            watch_path='',
            alias_path='',
            results_path='results',
            global_presql='',
            presql_file='pre.sql',
            events=[],
            eventprior='',
            width=DEFAULT_WIDTH):

        self.op_mode = op_mode

        self.dbms = dbms

        self.domain = domain

        self.config_path = config_path
        self.secrets_path = secrets_path
        self.secrets_key = secrets_key
        self.watch_path = watch_path

        self.alias_path = alias_path
        self.aliases = load_aliases.execute(alias_path)
        self.aliases = dynamic_aliases.execute(self.aliases)
        self.aliases.append(['domain', 'n', domain, 'all'])

        self.results_path = os.path.join(self.watch_path, results_path)
        ensure_path.execute(self.results_path)

        self.events = events
        self.eventprior = eventprior

        self.global_presql = global_presql
        self.presql_file = presql_file

        self.presql = get_presql.execute([global_presql, presql_file], self)

        self.width = width

    def subprocessor(self, sql_path):
        self.sql_path = sql_path
        processors.main.execute(self)

    def start(self):
        generate_watches.start(
            self.watch_path,
            self.events,
            self.eventprior,
            self.subprocessor)
