#!/usr/bin/env python3


# dependents

import os
from sty import fg, bg, ef, rs

from . import parse


# functions

def execute(meta_sql):

    sql_path = meta_sql.get('sql_path','')

    fn, fx = os.path.splitext(sql_path)
    if fx == '.sql':

        meta_sql['sql_path'] = os.path.dirname(sql_path)

        # read sql
        with open(sql_path, 'r') as fh:
            meta_sql['sql'] = [fh.read()]

        # sanitize sql
        meta_sql = parse.execute(meta_sql)

    meta_sql['codefile'] = fn
    return meta_sql
