#!/usr/bin/env python3

# dependents

import os
import pandas as pd
from sty import fg, bg, ef, rs
from tabulate import tabulate
from util import flatten

# functions

def execute(meta_sql):

    noshow = 0
    width = meta_sql['width']

    if 'df' in meta_sql:

        meta = meta_sql['meta']
        meta_flags = flatten.execute(
            [[m1.split(' ')[0] for m1 in m0 if m1 != ''] for m0 in meta if len(m0) > 0])

        if 'noshow' in meta_flags:
            noshow = 1

        if noshow == 0:

            # results

            print(fg.blue + 'RESULTS' + fg.rs + '\n')

            for df in meta_sql['df']:

                if isinstance(df, pd.DataFrame):
                    pd.set_option('display.max_columns', width)
                    pd.set_option('display.width', width)
                    pd.set_option('display.max_rows', None)

                    rc = len(df.index)
                    cc = len(df.columns)

                    index_mode = False
                    if cc > 2 * rc and rc < 5:
                        df = df.T
                        index_mode = True

                    # print(tabulate(df,showindex=False,headers=df.columns,tablefmt="simple"))
                    print(df)

                    print("{0} rows {1} columns returned.\n".format(rc, cc))

            # errors

            for err in meta_sql['error']:
                print(fg.red + err['sql_message'] + fg.rs + '\n')

            print(fg.blue + 'END' + fg.rs + '\n')
            print(fg.blue + meta_sql['codefile'] + fg.rs + '\n')
