#!/usr/bin/env python3


import os
import time
os.environ['TZ'] = 'Australia/Sydney'
time.tzset()


def execute():
    return time.strftime('%H:%M:%S %p %Z on %b %d, %Y')
