#!/usr/bin/env python3

import os


def execute(path):
    if not os.path.exists(path):
        os.makedirs(path)
