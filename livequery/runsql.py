#!/usr/bin/env python3


# dependents

import os
import pandas as pd
from sty import fg, bg, ef, rs
from py import exec_stmt
from py import comment
from . import select_lang


# functions

def execute(meta_sql, override=''):

    # statement index

    sql = meta_sql['sql_current']


    # language

    dbms = meta_sql.get('dbms','')
    meta_list = meta_sql.get('meta_current',[])
    lang = select_lang.execute(meta_list, dbms, override)


    # dbms and sql

    print(fg.li_green + lang + fg.rs)
    # print(fg.green + sql + fg.rs)


    for i,l in enumerate(sql.splitlines()):
        print(fg.green + str(i+1) + ' : ' + l + fg.rs)


    # exec sql

    if len(sql) > 0:

        results = {}

        if lang == 'gbq':
            results = meta_sql['gcloud'].bq_run_sql_df(sql)
        elif lang == 'rs':
            results = meta_sql['redshift'].run_sql_df(sql)
        elif lang == 'sp':
            results = meta_sql['spark'].run_sql_df(sql)
        elif lang == 'py':
            results, meta_sql = exec_stmt.execute(meta_sql,sql)
        elif lang == 'mmm':
            results = comment.execute(sql)

        meta_sql = {**meta_sql, **results}

    return meta_sql
