#!/usr/bin/env python3


import pattern


def execute(meta_sql, prefix):

    new_meta_sql = []

    for sql_text in meta_sql['sql']:

        new_sql_statement = []

        for ls in sql_text.splitlines():

            lst = ls.replace('(',' ( ').replace(')',' ) ').replace(',',' , ',).split(' ')
            lst = pattern.pipeline.execute(lst, prefix, __file__)
            ls = ' '.join(lst).replace(' ( ','(').replace(' ) ',')').replace(' , ',',')

            new_sql_statement.append(ls)

        new_meta_sql.append('\n'.join(new_sql_statement))

    meta_sql['sql'] = new_meta_sql

    return meta_sql

