#!/usr/bin/env python3


from util import find_index


def execute(text_list):

    f = find_index.execute('varchar',text_list)

    if f >= 0:
        text_list[f] = 'string'

        if text_list[f+1] == '(':
            text_list[f+1] = ''
            text_list[f+2] = ''
            text_list[f+3] = ''

        text_list = execute(text_list)

    return text_list
