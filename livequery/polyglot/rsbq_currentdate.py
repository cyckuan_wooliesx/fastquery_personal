#!/usr/bin/env python3


from util import find_index


def execute(text_list):

    f = find_index.execute('current_date',text_list)

    if f >= 0:
        text_list[f] = 'current_date()'
        text_list = execute(text_list)

    return text_list
