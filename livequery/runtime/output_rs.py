#!/usr/bin/env python3


from redshift import save_sql_df


def execute(params, meta_sql):

    results_path = meta_sql['results_path']
    df = meta_sql['df_current']

    conn_string = meta_sql['conn_string']
    conn_sslmode = meta_sql['conn_sslmode']
    conn_sslrootcert = meta_sql['conn_sslrootcert']
    conn_defaultschema = meta_sql['conn_defaultschema']

    schematable = params[0]

    print('redshift table ' + schematable + ' start save ...')

    save_sql_df(
        df,
        conn_string,
        conn_sslmode,
        conn_sslrootcert,
        conn_defaultschema,
        schematable)

    print('redshift table ' + schematable + ' saved.')
