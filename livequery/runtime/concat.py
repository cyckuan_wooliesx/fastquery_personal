#!/usr/bin/env python3

import pandas as pd


def execute(params, meta_sql):

    df1 = params[0]
    df2 = params[1]

    results = pd.concat([meta_sql['named_df'][df1], meta_sql['named_df'][df2]])

    meta_sql['df'].append(results)

    return meta_sql
