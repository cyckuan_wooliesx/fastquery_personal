#!/usr/bin/env python3


from jj import build_job


def execute(params, meta_sql):

    server_url = params[0]
    username = params[1]
    api_token = params[2]
    jobname = params[3]
    param_dict = eval(params[4])

    print(jobname)
    print(param_dict)

    build_job.execute(server_url, username, api_token, jobname, param_dict)

    return meta_sql
