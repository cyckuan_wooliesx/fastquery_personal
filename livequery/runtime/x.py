#!/usr/bin/env python3

import os
import sys
import subprocess


def execute(params, meta_sql):

    filename = os.path.join(os.path.dirname(meta_sql['sql_path']),params[0])
    fileext = os.path.splitext(filename)[1]

    print(f'''start calling {filename}''')

    try:
        subprocess.call([filename])
        # subprocess jupyter nbconvert --execute <notebook>
    except BaseException:
        # error handling not working
        # meta_sql not passed out
        meta_sql['sql_code'] = 'error'
        meta_sql['sql_message'] = str(sys.exc_info()[1])

    print(f'''end calling {filename}''')

    return meta_sql
