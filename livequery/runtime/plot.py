#!/usr/bin/env python3

from . import flatten
import seaborn as sns
import matplotlib.pyplot as plt
import os
import matplotlib as mpl


# functions

def execute(results, results_path):

    mpl.use('Agg')  # needed to address seg fault

    df_list = results['df']
    meta = results['meta']
    meta_segments = flatten(
        [[m1.split(' ') for m1 in m0 if m1 != ''] for m0 in meta if len(m0) > 0])

    for m in meta_segments:

        if m[0] in ['plotd', 'plott']:

            idx = int(m[1])
            d = df_list[idx]

            col = int(m[2])
            ld = d.ix[:, col]

            new = int(m[3])

            save_name = m[4]
            save_path = results_path + '/' + save_name

            port = m[5]

            axis = m[6:]

            if not isinstance(d, list):

                print(f'''\n*** preparing plot {save_name} ...''')

                # time series plot

                if m[0] == 'plott':

                    plot = sns.lineplot(
                        x="timepoint",
                        y="signal",
                        hue="region",
                        style="event",
                        data=ld)

                # joint density plot

                if m[0] == 'plotjd':

                    sns.set(style="white", color_codes=True)
                    plot = sns.jointplot(
                        x=ld[axis[0]], y=ld[axis[1]], kind='kde', color="skyblue")

                # cumulative density plot

                if m[0] == 'plotcd':

                    plt.ylabel('cumulative density')

                    plot = sns.distplot(
                        a=ld, hist_kws=dict(
                            cumulative=True), kde_kws=dict(
                            cumulative=True))

                # density plot

                if m[0] == 'plotd':

                    plt.ylabel('density')

                    plot = sns.distplot(a=ld)

                # figure

                if new > 0:
                    plt.figure()

                fig = plot.get_figure()
                fig.savefig(save_path)

                print(save_path + ' saved.')
                print(f'http://127.0.0.1:{port}/{save_name}')
