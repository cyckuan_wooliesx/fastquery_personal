from redshift import extract_ddl_sql
from livequery import sanitize_sql


def execute(sql_dir, ml, msql, params):

    schemaname, tablename = ml[1].split('.')

    fsql = extract_ddl_sql(schemaname, tablename)

    r = sanitize_sql(
        sql_file,
        sql,
        numeric_params=ll,
        aliases=params['aliases'])

    msql = msql + r['sql']

    return msql
