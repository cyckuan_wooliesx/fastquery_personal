#!/usr/bin/env python3


def execute(params, meta_sql):

    dfname = params[0]

    meta_sql.setdefault('named_df',dict())
    meta_sql['named_df'].update({dfname: meta_sql['df_current']})

    print(dfname + ' added.')

    return meta_sql
