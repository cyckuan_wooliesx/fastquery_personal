#!/usr/bin/env python3


def execute(params, meta_sql):

    gitpath = params[0]
    pipeline = params[1]

    print('\n' + gitpath + ' : ' + pipeline + ' run.')
