import os
import pandas as pd
import dask.dataframe as dd


def execute(sql_dir, ml, msql, params):

    filepath = ml[1]
    if ml[1][0] == '%':
        filepath = sql_dir + ml[1][1:]

    if ml[2] == '0':
        header = None
    else:
        header = int(ml[2])

    delimiter = ml[3]
    rowcount = int(ml[4])

    ddf = dd.read_csv(filepath, header=header, delimiter=delimiter)
    ddf.info()
    ddf.describe()

    print(ddf.count(axis=1))
    print(ddf.head(rowcount))

    return msql
