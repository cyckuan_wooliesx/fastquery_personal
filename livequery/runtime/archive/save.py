#!/usr/bin/env python3

import os
import csv
import pandas as pd
import pandas_gbq
import numpy as np

import sqlalchemy


def execute(params, meta_sql):

    results_path = meta_sql['results_path']
    df = meta_sql['df_current']

    save_format = params[0]
    save_name = params[1]

    save_path = f'''{results_path}/{save_name}.{save_format}'''

    # project_id should not be sourced here
    # print(df.dtypes)

    if save_format == 'gbq':
        pass
        # pandas_gbq.to_gbq(df, save_name, project_id=m[3], if_exists='replace')

    if save_format == 'rs':
        pass
        # save_sql_df(df, connection, save_name)

    if save_format == 'parquet':
        df.to_parquet(save_path,
                      engine='fastparquet',
                      compression='snappy',
                      index=False,
                      partition_cols=m[4:])

    if save_format == 'csv':
        df.to_csv(save_path, index=False)

    if save_format == 'pkl':
        df.to_pickle(save_path, index=False)

    if save_format in ['txt', 'sql', '']:
        np.savetxt(save_path, df.values, fmt='%s')

    print('\n' + save_path + ' saved.')
