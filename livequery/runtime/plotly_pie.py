#!/usr/bin/env python3


import pandas as pd
import plotly
import plotly.graph_objs as go

import fastflask


def execute(params, meta_sql):

    dfname, colx, coly, port = params

    df = meta_sql['named_df'][dfname]

    if colx.isdigit():
        colx = df.columns[int(colx)]

    if coly.isdigit():
        coly = df.columns[int(coly)]

    ids = [colx]

    graphs = [
        dict(
            data=[
                go.Pie(
                    labels=df[colx], values=df[coly]
                )
            ], layout={'title': dfname}
        )
    ]

    fastflask.plotly.execute(graphs, port)
