#!/usr/bin/env python3

import os
import csv
import pandas as pd
import pandas_gbq
import numpy as np

import sqlalchemy


def execute(params, meta_sql):

    df = meta_sql['df_current']
    project_id = meta_sql['gcloud_project']

    schematable = params[0]

    print('bigquery table ' + schematable + ' start save ...')

    pandas_gbq.to_gbq(
        df,
        schematable,
        project_id=project_id,
        if_exists='replace')

    print('bigquery table ' + schematable + ' saved.')
