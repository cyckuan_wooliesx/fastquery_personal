#!/usr/bin/env python3


import datetime


def execute(aliases):

    today = datetime.datetime.now()

    aliases.append(['currentdate', 'n', today.strftime('%Y%m%d'), 'all'])
    aliases.append(['currentts', 'n', today.strftime('%Y%m%d%H%M%S'), 'all'])

    return aliases
