#!/usr/bin/env python3


# dependent packages

import re
import os

from . import escapes
from . import constants


def execute(meta_sql):

    # sql path

    sql_dir = meta_sql.get('sql_path','')

    # statement processing

    meta_global_set = meta_sql['global']
    meta_list = meta_sql['meta']

    old_sql_list = meta_sql['sql']
    old_sql_list = [escapes.on(s) for s in old_sql_list]

    sql_list = []
    for i, s in enumerate(old_sql_list):

        s_singleline = re.sub('\n', ' ', s.strip().lower())
        s_top = re.sub(r'\([^)]*\)', '', s_singleline)
        s_words = s_top.split(' ')

        select_count = len([1 for w in s_words if w in constants.SELECT_WORDS])
        non_select_count = len([1 for w in s_words if w in constants.NON_SELECT_WORDS])
        limit_count = len([1 for w in s_words if w in constants.LIMIT])

        if 'nolimit' not in meta_global_set and select_count > 0 and non_select_count == 0 and limit_count == 0:
            s = s + '\nlimit 10'

        if len(s.strip()) > 0 and s.strip()[-1] != ';':
            s = s + '\n;'

        sql_list.append(s)

    sql_list = [escapes.off(s) for s in sql_list]

    meta_sql.update({
        "global": meta_global_set, "meta": meta_list, "sql": sql_list
    })

    return meta_sql
