#!/usr/bin/env python3


# dependents

import gcloud


# functions

def execute(meta_sql):

    g = gcloud.class_connection.connection(
        meta_sql['config_path'],
        meta_sql['secrets_path'],
        meta_sql['secrets_key'])

    meta_sql['gcloud'] = g
    meta_sql['gcloud_project'] = g.get_project()

    return meta_sql
