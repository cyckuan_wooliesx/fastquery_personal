#!/usr/bin/env python3


# dependents

import pyinotify


all_events = [
    "IN_CREATE",
    "IN_OPEN",
    "IN_ACCESS",
    "IN_ATTRIB",
    "IN_CLOSE_NOWRITE",
    "IN_CLOSE_WRITE",
    "IN_DELETE",
    "IN_DELETE_SELF",
    "IN_IGNORED",
    "IN_MODIFY",
    "IN_MOVE_SELF",
    "IN_MOVED_FROM",
    "IN_MOVED_TO",
    "IN_Q_OVERFLOW",
    "IN_UNMOUNT",
    "default"
]


# functions

class EventProcessor(pyinotify.ProcessEvent):
    prev_event = ''
    pass


def process_generator_donothing(cls, method):

    def _method_name(self, event):
        EventProcessor.prev_event = method
        # print(method)

    _method_name.__name__ = "process_{}".format(method)
    setattr(cls, _method_name.__name__, _method_name)


def process_generator(cls, method, eventprior, subprocessor):

    def _method_name(self, event):
        # print(method)
        if EventProcessor.prev_event == eventprior:
            subprocessor(event.pathname)

    _method_name.__name__ = "process_{}".format(method)
    setattr(cls, _method_name.__name__, _method_name)


# start watching

def start(watch_path, events, eventprior, subprocessor):

    for method in all_events:
        if method in events:
            process_generator(EventProcessor, method, eventprior, subprocessor)
        else:
            process_generator_donothing(EventProcessor, method)

    print('\nwatching ' + watch_path + ' ...\n')

    watch_manager = pyinotify.WatchManager()
    event_notifier = pyinotify.Notifier(watch_manager, EventProcessor())

    watch_manager.add_watch(watch_path, pyinotify.ALL_EVENTS)
    event_notifier.loop()
