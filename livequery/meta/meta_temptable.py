def execute(sql_dir, ml, msql, params):

    tablename = ml[1]

    if len(ml) > 2:
        tablealias = ml[2]
    else:
        tablealias = ''

    if params['lang'] == 'rs':
        msql.append(
            f'''{tablename} {tablealias}''')
    elif params['lang'] == 'gbq':
        msql.append(
            f'''temp.{tablename} {tablealias}''')

    return msql
