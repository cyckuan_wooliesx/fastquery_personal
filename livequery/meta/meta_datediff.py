def execute(sql_dir, ml, msql, params):

    # meta parameters start with ml[1], ml[2]

    date_unit = ml[1]
    date_1 = ml[2]
    date_2 = ml[3]

    if params['lang'] == 'rs':
        msql.append(f'''datediff({date_unit}, {date_1}, {date_2})''')
    elif params['lang'] == 'gbq':
        msql.append(f'''date_diff({date_2}, {date_1}, {date_unit})''')

    return msql
