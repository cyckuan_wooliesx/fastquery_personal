from redshift import extract_ddl_sql

def execute(sql_dir, ml, msql, params):

    schemaname, tablename = ml[1].split('.')

    sql = extract_ddl_sql.execute(schemaname, tablename)

    if params['lang'] == 'rs':
        msql.append(sql)

    return msql
