def execute(sql_dir, ml, msql, params):

    # meta parameters start with ml[1], ml[2]

    date_unit = ml[1]
    refdate = ml[2]

    if params['lang'] == 'rs':
        msql.append(f'''date_part({date_unit},{refdate})''')
    elif params['lang'] == 'gbq':
        msql.append(f'''extract({date_unit} from {refdate})''')

    return msql
