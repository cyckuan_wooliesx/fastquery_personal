def execute(sql_dir, ml, msql, params):

    tablename = ml[1]

    sql = f'''drop table if exists {tablename}; \ncreate temp table {tablename} ('''
    msql.append(sql)

    if len(ml) > 2:
        diststyle = ml[2]
        distkey = ml[3]

        if diststyle == 'key':
            msql.append(
                f'''%end ) diststyle {diststyle} distkey ({distkey})''')
        else:
            msql.append('%end )')
    else:
        msql.append('%end )')

    return msql
