def execute(sql_dir, ml, msql, params):

    stats_column = ml[1]
    stats_schematable = ml[2]

    msql.append(f'''
    select
    count(1) as c
    , count(distinct {stats_column}) as cd
    , min({stats_column}) as min_c
    , max({stats_column}) as max_c
    , avg({stats_column}) as mean_c
    from
    {stats_schematable}
    ;
    '''
                )

    return msql
