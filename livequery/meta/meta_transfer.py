import re
import gcloud.rsync_files as rsync


def execute(sql_dir, ml, msql, params):

    list_file = sql_dir + ml[1]

    with open(list_file, 'r') as lf:
        for l in lf.read().splitlines():
            l = l.strip()

            if l != '' and l[:2] != '--':
                if len(ml) > 2:
                    for idx, val in enumerate(ml[2:]):
                        l = re.sub(r'\$' + str(idx), val, l)

                src, dest = l.split(',')
                rsync(src, dest)

    return msql
