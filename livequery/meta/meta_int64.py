def execute(sql_dir, ml, msql, params):

    if params['lang'] == 'rs':
        msql.append(
            f'''bigint''')
    elif params['lang'] == 'gbq':
        msql.append(
            f'''int64''')

    return msql
