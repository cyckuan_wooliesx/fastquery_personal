def execute(sql_dir, ml, msql, params):

    tablename = ml[1]
    if len(ml) < 2:
        groupname = 'public'
    else:
        groupname = ml[2]

    if params['lang'] == 'rs':
        msql.append(f'''grant all on {tablename} to {groupname};''')

    return msql
