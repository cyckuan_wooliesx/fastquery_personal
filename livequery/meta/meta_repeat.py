import os
import re

# covers only 1 changing variable in the loop
# next upgrade - multiple variables


def execute(sql_dir, ml, msql, params):

    list_file = os.path.join(sql_dir, ml[1])
    loop_action = ' '.join(ml[2:]) + ';'

    with open(list_file, 'r') as lf:
        for l in lf.read().splitlines():
            l = l.strip()

            if l != '':
                ll = [le.strip() for le in l.split(',')]
                filled_sql = loop_action
                for idx, val in enumerate(ll):
                    filled_sql = re.sub(r'\$' + str(idx), val, filled_sql)
                msql.append(filled_sql)

    return msql
