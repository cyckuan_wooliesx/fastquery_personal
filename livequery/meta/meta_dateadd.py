def execute(sql_dir, ml, msql, params):

    # meta parameters start with ml[1], ml[2]

    refdate = ml[1]
    days = ml[2]
    date_unit = ml[3]

    if params['lang'] == 'rs':
        msql.append(f'''dateadd({date_unit},{days},{refdate})''')
    elif params['lang'] == 'gbq':
        msql.append(f'''date_add({refdate}, interval {days} {date_unit})''')

    return msql
