def execute(sql_dir, ml, msql, params):

    tablename = ml[1]

    if params['lang'] == 'rs':

        if len(ml) > 2:
            diststyle = ml[2]
            distkey = ml[3]

            if diststyle == 'key':
                sql = f'''drop table if exists {tablename}; \ncreate temp table {tablename} diststyle {diststyle} distkey ({distkey}) as'''
            else:
                sql = f'''drop table if exists {tablename}; \ncreate temp table {tablename} diststyle {diststyle} as'''

        else:
            sql = f'''drop table if exists {tablename}; \ncreate temp table {tablename} as'''

    elif params['lang'] == 'gbq':

        sql = f'''create or replace table temp.{tablename} as'''

    msql.append(sql)

    return msql
