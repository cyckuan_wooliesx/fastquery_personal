def execute(sql_dir, ml, msql, params):

    # meta parameters start with ml[1], ml[2]

    if params['lang'] == 'rs':
        msql.append(
            f'''rewrite in redshift dialect''')
    elif params['lang'] == 'gbq':
        msql.append(
            f'''rewrite in bigquery dialect''')

    return msql
