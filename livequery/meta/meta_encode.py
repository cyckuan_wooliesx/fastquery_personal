def execute(sql_dir, ml, msql, params):

    if len(ml) == 1:
        format = 'zstd'
    else:
        format = ml[1]

    if params['lang'] == 'rs':
        msql.append(
            f'''encode {format}''')

    return msql
