def execute(sql_dir, ml, msql, params):

    if params['lang'] == 'rs':
        msql.append(
            f'''sysdate''')
    elif params['lang'] == 'gbq':
        msql.append(
            f'''current_datetime()''')

    return msql
