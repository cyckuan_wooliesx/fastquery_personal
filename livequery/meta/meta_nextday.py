def execute(sql_dir, ml, msql, params):

    date_arg = ' '.join(ml[2:])
    weekday = ml[1]

    if params['lang'] == 'rs':
        weekday = weekday.title()
        msql.append(
            f'''next_day({date_arg},'{weekday}')''')
    elif params['lang'] == 'gbq':
        msql.append(
            f'''date_add(date_trunc({date_arg},week({weekday})),interval 7 day)''')

    return msql
