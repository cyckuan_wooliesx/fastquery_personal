import re

from livequery import sanitize_sql


def execute(sql_dir, ml, msql, params):

    template_file = sql_dir + ml[1]

    with open(template_file, 'r') as tf:
        template_sql = tf.read()

    ll = []
    if len(ml) > 2:
        ll = ml[2:]

    r = sanitize_sql(
        template_file,
        template_sql,
        numeric_params=ll,
        aliases=params['aliases'])
    msql = msql + r['sql']

    return msql
