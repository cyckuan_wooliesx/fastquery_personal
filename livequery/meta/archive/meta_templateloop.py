import re

from livequery import sanitize_sql


def execute(sql_dir, ml, msql, params):

    list_file = sql_dir + ml[1]
    template_file = sql_dir + ml[2]

    with open(template_file, 'r') as tf:
        template_sql = tf.read()

    with open(list_file, 'r') as lf:
        for l in lf.read().splitlines():
            l = l.strip()

            if l != '' and l[:2] != '--':
                if len(ml) > 3:
                    for idx, val in enumerate(ml[3:]):
                        l = re.sub(r'\$' + str(idx), val, l)

                ll = [le.strip() for le in l.split(',')]

                r = sanitize_sql(
                    template_file,
                    template_sql,
                    numeric_params=ll,
                    aliases=params['aliases'])
                msql = msql + r['sql']

    return msql
