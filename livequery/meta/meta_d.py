def execute(sql_dir, ml, msql, params):

    tablename = ml[1]

    msql.append(f'''drop table if exists {tablename};''')

    return msql
