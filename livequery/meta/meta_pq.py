
import fastparquet


def execute(sql_dir, ml, msql, params):

    filepath = sql_dir + ml[1]
    rowcount = int(ml[2])

    ddf = fastparquet.ParquetFile(filepath).to_pandas()

    print(ddf.shape)
    print(ddf.head(rowcount))

    return msql
