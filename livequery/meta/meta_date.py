def execute(sql_dir, ml, msql, params):

    date_param = ml[1]

    if params['lang'] == 'rs':
        msql.append(f'''{date_param}''')
    elif params['lang'] == 'gbq':
        msql.append(f'''cast({date_param} as datetime)''')

    return msql
