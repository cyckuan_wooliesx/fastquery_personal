import re


def execute(sql_dir, ml, msql, params):

    global aliases

    target_alias = ml[1]
    target_value = ml[2]

    print('aliases before:')
    print(params['aliases'])

    found = 0
    for a in params['aliases']:
        if a[0] == target_alias:
            a[2] = target_value
            found = 1

    if found == 0:
        params['aliases'].append([target_alias, 'n', target_value])

    print('aliases after:')
    print(params['aliases'])

    aliases = params['aliases']

    return msql
