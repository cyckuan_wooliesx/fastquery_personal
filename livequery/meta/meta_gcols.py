def execute(sql_dir, ml, msql, params):

    cols_schema, cols_table = ml[1].split('.')
    msql.append(
        f'''select column_name, data_type from {cols_schema}.INFORMATION_SCHEMA.COLUMN_FIELD_PATHS where table_name = '{cols_table}' limit 10000;''')

    return msql
