def execute(sql_dir, ml, msql, params):

    tablename = ml[1]

    sql = f'''insert into {tablename} '''
    msql.append(sql)

    if params['lang'] == 'rs':
        msql.append(f'''%end \n;\ninsert\ninto\n%sys.control_log\nselect sysdate, '{tablename}', 'insert',  count(1) from {tablename}\n;''')
    elif params['lang'] == 'gbq':
        msql.append(f'''%end \n;\ninsert\ninto\n%sys.control_log\nselect current_datetime(), '{tablename}', 'create', count(1) from {tablename}\n;''')

    return msql
