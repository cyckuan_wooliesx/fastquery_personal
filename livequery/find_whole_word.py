import re


def execute(needle, haystack):
    return re.search(
        r"\b" +
        re.escape(
            needle.lower()) +
        r"\b",
        haystack.lower())
