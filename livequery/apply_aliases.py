#!/usr/bin/env python3

import re
import os
import datetime

postdelim = [['_', '_'], [' ', ' '], ['/', '/'],
             ['\.', '.'], ['\'', "'"], [';', ';'], ['\n', '\n']]

META_CHARS = ['%', '$']


# need better handling inside and outside quotes

def execute(sql, aliases):

    for mc in META_CHARS:

        # meta tags with end delimiter
        for alias, mode, fulltext, dbms in aliases:
            for dp in postdelim:
                alias = mc + alias + dp[0]
                if mode == 's':
                    replacement = f''' {fulltext} '''
                elif mode == 'd':
                    replacement = datetime.datetime.now().strftime(fulltext) + dp[1]
                else:
                    replacement = fulltext + dp[1]

                sql = re.sub(alias, replacement, sql)

        # meta tags without end delimiter
        for alias, mode, fulltext, dbms in aliases:
            alias = mc + alias

            if mode == 'd':
                replacement = datetime.datetime.now().strftime(fulltext)
            else:
                replacement = fulltext

            sql = sql.replace(alias, replacement)

    return sql
