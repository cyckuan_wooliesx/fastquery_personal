#!/usr/bin/env python3


from livequery import redshift_connect
from livequery import error_conn
from livequery import runsql


def execute(meta_sql):

    meta_sql = redshift_connect.execute(meta_sql)

    if meta_sql['conn_code'] == 'error':
        error_conn.execute(meta_sql)
        meta_sql['stop'] = 1
    else:
        for s in meta_sql['presql']:
            meta_sql['sql_current'] = s
            runsql.execute(meta_sql,override='rs')

    return meta_sql
