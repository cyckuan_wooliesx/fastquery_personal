#!/usr/bin/env python3


import os

from livequery import readsql
from livequery import argsql


def execute(meta_sql):

    sql_path = meta_sql['sql_path']

    if isinstance(sql_path, str) and os.path.isfile(sql_path):
        if os.path.splitext(sql_path)[1] == '.sql':
            meta_sql = readsql.execute(meta_sql)
        else:
            return None
    else:
        meta_sql = argsql.execute(meta_sql)

    return meta_sql
