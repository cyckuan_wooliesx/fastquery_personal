#!/usr/bin/env python3


from livequery import carmageddon


def execute(meta_sql):

    carmageddon.execute('runtime', '\\', meta_sql)

    return meta_sql
