#!/usr/bin/env python3


from livequery import gcloud_connect


def execute(meta_sql):

    meta_sql = gcloud_connect.execute(meta_sql)

    return meta_sql
