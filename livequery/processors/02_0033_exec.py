#!/usr/bin/env python3

import os
import util

from livequery import readable_time
from livequery import runsql

from livequery import commit
from livequery import rollback
from livequery import display_results
from livequery import error_sql
from livequery import error_none
from livequery import error_stop



def execute(meta_sql):

    if len(meta_sql['sql_current']) > 0:

        # sql statement exec

        print('start: ' + readable_time.execute())
        results = runsql.execute(meta_sql)
        print('end: ' + readable_time.execute())
        print(f'''runtime: {results['elapsed']} seconds''')

        # post-proc

        if 'df' in results:

            # commit between statements

            if results['df'] is not None:
                meta_sql['df'].append(results['df'])
                meta_sql["df_current"] = results["df"]
                commit.execute(meta_sql)
            else:
                meta_sql["df_current"] = None

            # display results

            display_results.execute(results)

            # error

            if results['sql_code'] == 'error':
                error_sql.execute(results)
                meta_sql['error'].append(results)
                rollback.execute(results)
                util.logentry.execute(os.path.join(meta_sql['results_path'],'fail.log'),'%s %s' % (meta_sql['domain'], meta_sql['sql_current']))
            else:
                error_none.execute(results)
                util.logentry.execute(os.path.join(meta_sql['results_path'],'success.log'),'%s %s' % (meta_sql['domain'], meta_sql['sql_current']))

            # stop

            if 'stop' in results:
                error_stop.execute(results)
                meta_sql['stop'] = 1
                util.logentry.execute(os.path.join(meta_sql['results_path'],'stop.log'),'%s %s' % (meta_sql['domain'], meta_sql['sql_current']))

    return meta_sql
