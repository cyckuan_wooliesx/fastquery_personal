#!/usr/bin/env python3


# dependents

import os

import pattern


def execute(lq):

    meta_sql = lq.__dict__
    meta_sql.update({
        'df': [], 'error': [], 'meta': []
    })

    meta_sql = pattern.pipeline.execute(meta_sql,'01',__file__)

    return meta_sql
