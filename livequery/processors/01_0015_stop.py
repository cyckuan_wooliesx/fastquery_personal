#!/usr/bin/env python3


import util
import os
from livequery import error_stop


def execute(meta_sql):

    if 'stop' in meta_sql:
        error_stop.execute(meta_sql)
        util.logentry.execute(os.path.join(meta_sql['results_path'],'stop.log'),'%s %s' % (meta_sql['domain'], meta_sql['stop']))
        util.logentry.execute(os.path.join(meta_sql['results_path'],'stop.log'),'%s %s' % (meta_sql['domain'], meta_sql['sql']))

    return meta_sql
