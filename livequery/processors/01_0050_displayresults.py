#!/usr/bin/env python3


from livequery import display_results_all


def execute(meta_sql):

    display_results_all.execute(meta_sql)

    return meta_sql
