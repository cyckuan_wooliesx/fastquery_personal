#!/usr/bin/env python3


from livequery import commit
from livequery import close_connection


def execute(meta_sql):

    commit.execute(meta_sql)
    close_connection.execute(meta_sql)

    return meta_sql
