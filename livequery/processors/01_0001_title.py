#!/usr/bin/env python3


import util


def execute(meta_sql):

    util.figlet_title.execute('univers', meta_sql['domain'])

    return meta_sql
