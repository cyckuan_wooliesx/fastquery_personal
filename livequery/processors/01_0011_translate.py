#!/usr/bin/env python3


TRANSLATE_MODES = ['translate','watch_translate']


import os

def execute(meta_sql):

    if meta_sql['op_mode'] in TRANSLATE_MODES:

        outfile = '.'.join([os.path.basename(meta_sql['codefile']),meta_sql['domain'],'translated.sql'])
        outfile = os.path.join(meta_sql['results_path'],outfile)

        with open(outfile,'w') as outf:
            for sql in meta_sql['sql']:
                outf.write(sql+'\n')
                outf.write('\n\n')
            outf.close()

        meta_sql['stop'] = 'translation only'

    return meta_sql

