#!/usr/bin/env python3

RUN_MODES = ['run', 'watch_run']

import pattern


def execute(meta_sql):

    # looping statements

    if meta_sql['op_mode'] in RUN_MODES:

        for n, s in enumerate(meta_sql['sql']):

            meta_sql['sql_current'] = s
            meta_sql['meta_current'] = meta_sql['meta'][n]

            meta_sql = pattern.pipeline.execute(meta_sql,'02',__file__)

            if 'stop' in meta_sql:
                break

    return meta_sql
