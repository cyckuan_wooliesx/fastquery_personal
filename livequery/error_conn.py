#!/usr/bin/env python3


# dependents

import os
import pandas as pd
from sty import fg, bg, ef, rs


# functions

def execute(meta_sql):

    if 'conn_message' in meta_sql:
        print(fg.red + '\n*** CONNECTION ERROR ***\n' + fg.rs)
