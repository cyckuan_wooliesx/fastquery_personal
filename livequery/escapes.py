#!/usr/bin/env python3

import re

escape_list = [['\-\-', 'comment', '--'],
               [';', 'semicolon', ';'], ['\%', 'percentage', '%']]


def on(text):

    for e in escape_list:
        tag = '<' + e[1] + '>'
        text = re.sub(r'(?!(([^"]*"){2})*[^"]*$)' + e[0], tag, text)
        text = re.sub(r"(?!(([^']*'){2})*[^']*$)" + e[0], tag, text)

    return text


def off(text):

    for e in escape_list:
        tag = '<' + e[1] + '>'
        text = text.replace(tag, e[2])

    return text
