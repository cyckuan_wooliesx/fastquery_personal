#!/usr/bin/env python3


# dependents

from . import parse


# functions

def execute(meta_sql):

    meta_sql = parse.execute(meta_sql)
    meta_sql['codefile'] = 'arg'

    return meta_sql
