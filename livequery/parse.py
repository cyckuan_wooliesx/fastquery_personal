#!/usr/bin/env python3


# dependent packages

import re
import os

from . import recurse_clean
from . import statement_meta
from . import statement_post
from . import polyglot


PREFIX = 'rsbq'


def execute(meta_sql):

    # sql path

    statement_count = 0
    statement_change = 1

    pass_count = 0

    # multi pass processing

    while statement_change > 0 and pass_count < 2:

        pass_count += 1

        # recurse file build
        if pass_count == 1:
            meta_sql = recurse_clean.execute(meta_sql)

        # statement meta
        meta_sql = statement_meta.execute(meta_sql)

        # statement meta
        meta_sql = statement_post.execute(meta_sql)

        # change

        statement_change = len(meta_sql['sql']) - statement_count
        statement_count = len(meta_sql['sql'])

        # debug

        # print(f'''\npass : {pass_count}''')
        # print(meta_sql['sql'])
        # print(meta_sql['meta'])

    if meta_sql['lang'] == 'gbq':
        meta_sql = polyglot.translator.execute(meta_sql, PREFIX)

    return meta_sql
