#!/usr/bin/env python3

# dependents

import os
import pandas as pd
from tabulate import tabulate
from sty import fg, bg, ef, rs

from util import flatten


# functions

def execute(meta_sql):

    noshow = 0
    width = meta_sql['width']

    if meta_sql['sql_code'] == 'success' and 'df' in meta_sql:

        meta = meta_sql['meta']
        meta_flags = flatten.execute(
            [[m1.split(' ')[0] for m1 in m0 if m1 != ''] for m0 in meta if len(m0) > 0])

        if 'noshow' in meta_flags:
            noshow = 1

        df = meta_sql['df']

        if df is not None and noshow == 0:

            df.index += 1

            pd.set_option('display.max_columns', width)
            pd.set_option('display.max_colwidth', width)
            pd.set_option('display.width', width)

            with pd.option_context('display.max_rows', None, 'display.max_columns', None):
                print(tabulate(df, showindex=False, headers=df.columns, tablefmt="simple"))

            print("\n{0} rows {1} columns returned.".format(
                len(df.index), len(df.columns)))
