#!/usr/bin/env python3


import re
import os

from . import escapes
from . import apply_aliases
from cleancode import remove_sql_comments


def execute(meta_sql):

    # inputs

    new_meta_sql = []
    aliases = meta_sql['aliases']
    numeric_params = meta_sql.get('numeric_params',[])

    for fsql in meta_sql['sql']:

        # escapes in quotes

        fsql = escapes.on(fsql)

        # strip comments

        fsql = remove_sql_comments.execute(fsql)

        # replace aliases

        fsql = apply_aliases.execute(fsql, aliases)

        # replace numeric params

        for idx, val in enumerate(numeric_params):
            fsql = re.sub(r'\$' + str(idx), val, fsql)

        # escapes in quotes

        fsql = escapes.off(fsql)

        new_meta_sql.append(fsql)

    meta_sql['sql'] = new_meta_sql

    return meta_sql
