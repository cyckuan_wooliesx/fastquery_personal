#!/usr/bin/env python3

LANG_LIST = ['rs','gbq','py','spark','mmm']

def execute(meta_list, dbms, override=''):

    lang = ''
    if override != '':
        lang = override
    else:
        meta_lang = [l for l in meta_list if l in LANG_LIST]
        if len(meta_lang) > 0:
            lang = meta_lang[0]

    if lang == '':
        lang = dbms

    return lang


