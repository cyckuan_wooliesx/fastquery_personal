#!/usr/bin/env python3


# dependent packages

import os

from . import recurse_clean


# constants

META_CHARS = ['%', '$']


def execute(meta_sql):

    # rebuilder

    sql_dir = meta_sql.get('sql_path','')

    old_meta_sql = meta_sql['sql'].copy()
    new_meta_sql = []

    for fsql in old_meta_sql:

        for l in fsql.split('\n'):

            ls = l.strip()

            if len(ls) > 0 and ls[0] not in META_CHARS and ls[-4:] == '.sql':

                sql_file = os.path.join(sql_dir,ls)

                if os.path.exists(sql_file):
                    with open(sql_file, 'r') as sqlf:
                        meta_sql['sql'] = [sqlf.read()]
                        meta_sql['sql_path'] = sql_file
                        meta_sql = recurse_clean.execute(meta_sql)
                        new_meta_sql.extend(meta_sql['sql'])

            else:
                new_meta_sql.extend([l])

    new_meta_sql = '\n'.join(new_meta_sql)
    meta_sql['sql'] = [new_meta_sql]

    return meta_sql
