#!/usr/bin/env python3


# dependents

import os
from sty import fg, bg, ef, rs


# functions

def execute(meta_sql):

    if 'stop' in meta_sql:
        print(fg.red + '\n*** STOPPED ***\n' + fg.rs)
        print(fg.red + meta_sql['stop'] + fg.rs + '\n\n')
