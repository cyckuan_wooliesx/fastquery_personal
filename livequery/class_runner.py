#!/usr/bin/env python3


# dependents

import os

from importhelp import importall
__all__ = importall.load_all_modules(__package__, __file__, globals())

from . import processors


# defaults

DEFAULT_WIDTH = 150


# run

class runner():

    def __init__(
            self,
            op_mode,
            dbms,
            domain,
            config_path,
            secrets_path,
            secrets_key,
            alias_path,
            results_path='results',
            global_presql='',
            presql_file='pre.sql',
            width=DEFAULT_WIDTH):

        self.op_mode = op_mode

        self.dbms = dbms

        self.domain = domain
        self.config_path = config_path
        self.secrets_path = secrets_path
        self.secrets_key = secrets_key

        self.alias_path = alias_path
        self.aliases = load_aliases.execute(alias_path)
        self.aliases = dynamic_aliases.execute(self.aliases)
        self.aliases.append(['domain', 'n', domain,'all'])

        self.results_path = results_path

        self.global_presql = global_presql
        self.presql_file = presql_file
        self.presql = get_presql.execute([global_presql, presql_file], self)

        self.width = width


    def start(self, sql):
        self.sql_path = sql
        self.sql = sql
        if os.path.isfile(sql):
            ensure_path.execute(os.path.join(os.path.dirname(sql), self.results_path))

        result = processors.main.execute(self)

        return result
