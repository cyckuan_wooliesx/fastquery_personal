#!/usr/bin/env python3


import os


def execute(alias_path):

    aliases = []

    if alias_path == '':
        return aliases

    for file in os.listdir(alias_path):

        fullpath = os.path.join(alias_path, file)

        with open(fullpath, 'r') as af:
            aliases = aliases + [[b.strip() for b in a.split(',')]
                                 for a in af.read().splitlines() if a.strip() != '']

    return aliases
