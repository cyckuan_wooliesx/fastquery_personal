#!/usr/bin/env python3


# dependents

import os
import pandas as pd
from sty import fg, bg, ef, rs


# functions

def execute(meta_sql):

    if 'sql_message' in meta_sql:
        print(fg.red + '\n*** SQL ERROR ***\n' + fg.rs)
        print(fg.red + meta_sql['sql_message'] + fg.rs + '\n\n')
