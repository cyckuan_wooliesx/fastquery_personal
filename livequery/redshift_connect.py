#!/usr/bin/env python3


# dependents

import redshift


# functions

def execute(meta_sql):

    r = redshift.class_connection.connection(
        meta_sql['config_path'],
        meta_sql['secrets_path'],
        meta_sql['secrets_key'])
    r.connect()

    meta_sql['redshift'] = r

    meta_sql['conn_string'] = r.get_connection_string()
    meta_sql['conn_sslmode'] = r.get_sslmode()
    meta_sql['conn_sslrootcert'] = r.get_sslrootcert()
    meta_sql['conn_defaultschema'] = r.get_defaultschema()

    meta_sql['conn_code'] = r.get_connection_code()
    meta_sql['conn_message'] = r.get_connection_message()

    return meta_sql
