#!/usr/bin/env python3


# dependent packages

import re
import os

from . import escapes
from . import metastasize
from . import apply_aliases
from util import flatten


def execute(meta_sql):

    # config

    aliases = meta_sql['aliases']

    # statement processing

    old_global = meta_sql.get('global',set())
    old_meta = meta_sql.get('meta',[[]])
    old_sql = meta_sql['sql']

    new_sql = []
    new_meta = []
    new_global = set()

    # main process

    for n, e_sql in enumerate(old_sql):

        e_sql = escapes.on(e_sql)

        if len(old_meta) == 0:
            e_meta = []
        else:
            e_meta = old_meta[n]

        elim = 0
        # eliminate if no sql or meta
        if len(e_sql) == 0 and len(e_meta) == 0:
            elim = 1
        # meta only no sql
        elif e_sql == '':
            f_sql = ['']
            f_meta = [e_meta]
        # sql and maybe meta
        else:
            f_sql = []
            f_meta = []

            for s in e_sql.split(';'):

                s = s.strip()
                if s != '':
                    meta_sql['statement'] = s.strip()
                    s_meta, s_sql = metastasize.execute(meta_sql)
                    s_sql = apply_aliases.execute(s_sql, aliases)
                    s_sql = escapes.off(s_sql)

                    f_sql.append(s_sql)
                    f_meta.append(e_meta + s_meta)

        # add to results
        if elim == 0:
            new_sql.extend(f_sql)
            new_meta.extend(f_meta)
            new_global = new_global.union(set(flatten.execute(f_meta)))

    # consolidate result

    meta_sql.update({
        "global": new_global, "meta": new_meta, "sql": new_sql
    })

    return meta_sql
