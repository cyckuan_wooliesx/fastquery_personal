#!/usr/bin/env python3


# dependent packages

import re


# regex functions

REG_META = re.compile(r"(\<)(?!.*\n.*\>).*(\>)", re.I)

REG_BLOCK_COMMENT = re.compile(r"(/\*)[\w\W]*?(\*/)", re.I)
REG_LINE_COMMENT = re.compile('(--.*)', re.I)
REG_BRACKETS = re.compile(r"\[|\]|\)|\"", re.I)
REG_PARENTS = re.compile(
    r"(?<=join\s)+[\S\.\"\']+|(?<=from\s)+[\S\.\"\']+", re.I)
REG_CTES = re.compile(r"(\S+)\sas\W*\(", re.I)


def execute(sql):
    sql = REG_BLOCK_COMMENT.sub('', sql)  # remove block comments
    sql = REG_LINE_COMMENT.sub('', sql)  # remove line comments
    return sql
