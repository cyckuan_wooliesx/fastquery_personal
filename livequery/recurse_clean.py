#!/usr/bin/env python3


# dependent packages

import re
import os

from . import sanitize
from . import recurse_insert


def execute(meta_sql):

    # late dependency

    # from . import recurse_insert

    # sanitize

    meta_sql = sanitize.execute(meta_sql)

    # import files

    meta_sql = recurse_insert.execute(meta_sql)

    return meta_sql
