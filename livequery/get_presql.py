#!/usr/bin/env python3


# dependents

import os
from . import readsql


# functions

def execute(presql_paths, lq):

    meta_sql = lq.__dict__

    presql = []
    for fp in presql_paths:
        meta_sql['sql_path'] = fp
        if os.path.exists(fp):
            meta_sql = readsql.execute(meta_sql)
            presql = presql + meta_sql['sql']

    presql = list(set(presql))
    presql = [s for s in presql if s.strip() != '']

    return presql
