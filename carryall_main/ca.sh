#!/usr/bin/env bash


# config

wd="$( cd "$(dirname "$0")" ; pwd -P )"
config="/home/$USER/fastquery_scaffold/config/config.prod.yaml"
secrets="/home/$USER/fastquery_scaffold/config/secrets2.prod.yaml"
secrets_key="/home/$USER/fastquery_scaffold/config/lq.key"
mains="/home/$USER/lib/fastquery/carryall_main"


# external params

mode=$1
flow=$2
source_tables="$wd/$3"
loadmode=$4


# param expansion

declare -a array=(
'(lm2 loyalty_modeling loyalty_modeling)'
'(l2 loyalty loyalty)'
'(lmb loyalty_modeling bwsre)'
'(lb loyalty bwsre)'
)

for elt in "${array[@]}"; do
    eval "params=$elt"
    if [ "$flow" == "$params" ]; then
      source_schema=${params[1]}
      dest_dataset=${params[2]}
    fi
done


# execution

declare -a array=(
  '("^(s|x|xy)$" "$mains/ca3_schema.py $config $secrets $secrets_key $source_schema $source_tables")'
  '("^(c|x|xy)$" "$mains/ca3_create_tables.py $config $secrets $secrets_key $dest_dataset $source_schema $source_tables")'
  '("^(u|y|xy)$" "$mains/ca3_unload.py $config $secrets $secrets_key $source_schema $source_tables")'
  '("^(t|y|xy)$" "$mains/ca3_transfer.py $config $secrets $secrets_key $source_schema $source_tables")'
  '("^(l|y|xy)$" "$mains/ca3_load.py $config $secrets $secrets_key $dest_dataset $source_schema $source_tables $loadmode")'
)

for elt in "${array[@]}"; do
    eval "params=$elt"
    if eval "[[ "$mode" =~ $params ]]"
    then
        eval ${params[1]}
    fi
    echo ${params[@]}
done



