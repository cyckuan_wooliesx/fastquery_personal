#!/usr/bin/env python3


import carryall


def main(args):

  carryall.pipeline.execute(args.config, args.secrets, args.secrets_key, 'unload', carryall.process_unload.execute, tablelist_path=args.tablelist, schemaname=args.schema)

  return 0



# args

import argparse
ap = argparse.ArgumentParser(
     description=__doc__,
     formatter_class=argparse.RawDescriptionHelpFormatter)

ap.add_argument('config', help='Name of Redshift configuration file.')
ap.add_argument('secrets', help='Name of secrets file.')
ap.add_argument('secrets_key', help='Name of secrets key file.')
ap.add_argument('schema', help='Name of schema.')
ap.add_argument('tablelist', help='File listing tables.')



# main and arg

if __name__ == '__main__':
    main(ap.parse_args())

exit

