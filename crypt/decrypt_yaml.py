#!/usr/bin/env python3


from . import generate_key


import yaml


def execute(password, config_yaml):

    key = generate_key.execute(password)
    print("key : " + key.decode('utf-8'))

    with open(config_yaml, 'r') as db_config_file:
        cfg = yaml.load(db_config_file, Loader=yaml.FullLoader)

    for k in cfg:
        print(k)
        print(cfg[k])
        print(decrypt_message(cfg[k], key))
