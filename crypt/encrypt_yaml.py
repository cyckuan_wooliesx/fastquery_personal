#!/usr/bin/env python3


from . import generate_key
from . import encrypt_message


import yaml


def execute(password, config_in, config_out):

    key = generate_key.execute(password)
    print("key : " + key.decode('utf-8'))

    with open(config_in, 'r') as config_file:
        cfg = yaml.load(config_file, Loader=yaml.FullLoader)

    new_cfg = dict()
    for k in cfg:
        new_cfg[k] = encrypt_message.execute(cfg[k], key)
        print('\n'.join([k, cfg[k], new_cfg[k]]))

    with open(config_out, 'w') as config_file:
        yaml.dump(new_cfg, config_file, default_flow_style=False)
