#!/usr/bin/env python3

from . import generate_key
from . import encrypt_message


def execute(password, secrets):

    key = generate_key.execute(password)
    print("key : " + key.decode('utf-8'))

    with open(key, 'w') as f:
        f.write(key)

    for s in secrets:
        encrypted = encrypt_message.execute(s, key)
        print(s)
        print(encrypted)
        print()
