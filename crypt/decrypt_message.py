#!/usr/bin/env python3


from . import ensure_byte
from cryptography.fernet import Fernet


def execute(encrypted_message, key):

    key = ensure_byte.execute(key)

    encrypted_encoded = encrypted_message.encode()
    f = Fernet(key)
    decrypted = f.decrypt(encrypted_encoded)

    return decrypted.decode('utf-8')
