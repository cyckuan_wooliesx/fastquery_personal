#!/usr/bin/env python3


from cryptography.fernet import Fernet
from . import ensure_byte


def execute(message, key):

    key = ensure_byte.execute(key)

    message_encoded = message.encode()
    f = Fernet(key)
    encrypted = f.encrypt(message_encoded)
    return encrypted.decode('utf-8')
