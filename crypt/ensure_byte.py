#!/usr/bin/env python3


def execute(key):

    try:
        key = key.encode()
    except (UnicodeDecodeError, AttributeError):
        pass

    return key
