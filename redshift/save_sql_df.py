#!/usr/bin/env python3


# dependent packages

from psycopg2.extras import NamedTupleCursor
from sqlalchemy import create_engine


# runs redshift sql and returns cursor

def execute(
        df,
        connection_string,
        sslmode,
        sslcert,
        default_schema,
        schematable):

    conn = create_engine(
        'redshift+psycopg2://' +
        connection_string,
        connect_args={
            'sslmode': sslmode,
            'sslrootcert': sslcert,
            'options': '-csearch_path={}'.format(default_schema)})
    df.to_sql(schematable, conn, index=False, if_exists='replace')
    return 0
