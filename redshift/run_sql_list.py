#!/usr/bin/env python3


# dependent packages

import pandas as pd
from psycopg2.extras import NamedTupleCursor

from . import run_sql_df


# runs redshift sql and returns cursor


def execute(connection, sql):
    results = {}
    results = run_sql_df.execute(connection, sql)
    return results['df'].tolist()
