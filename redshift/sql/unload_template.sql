
UNLOAD('{sql}')
TO '{s3location}'
CREDENTIALS 'aws_access_key_id={secrets['awsaccesskey']};aws_secret_access_key={secrets['awssecretkey']}'
DELIMITER '{unloaddelimiter}'
NULL AS '{nullcharacter}'
{addquotes}
{header}
{escape}
{unloadformat}
{maxfilesize}
PARALLEL {parallel}
ALLOWOVERWRITE
;
