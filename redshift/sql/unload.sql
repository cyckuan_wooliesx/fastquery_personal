
UNLOAD('select * from {schemaname}.{tablename} {whereclause}')
TO '{config['s3repo']}/{config['dataprefix']}/{schemaname}/{tablename}/{partition}/'
CREDENTIALS 'aws_access_key_id={secrets['awsaccesskey']};aws_secret_access_key={secrets['awssecretkey']}'
DELIMITER '{unloaddelimiter}' {config['unloadformat']}
NULL AS '{config['nullcharacter']}'
ESCAPE
ALLOWOVERWRITE
MAXFILESIZE AS {config['unloadfilesize']}
ADDQUOTES
PARALLEL ON
;
