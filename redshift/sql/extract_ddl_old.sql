
SELECT
*
FROM
pg_table_def
WHERE
schemaname = 'loyalty'
AND tablename = 'article_sales_summary'
;




SELECT
n.nspname
, c.relname
, c.relkind

FROM
pg_namespace AS n
INNER JOIN  pg_class AS c ON n.oid = c.relnamespace
INNER JOIN  pg_attribute AS a ON c.oid = a.attrelid

WHERE
nspname = 'loyalty'
AND relname = 'article_sales_summary'

-- GROUP BY
-- 1,2

LIMIT
100
;
