
#!/usr/bin/env python3


from importhelp import importall
__all__ = importall.load_all_modules(__package__, __file__, globals())

import config



class connection(object):

    def __init__(self, config_path, secrets_path, secrets_key):
        self.config_path = config_path
        self.read_config()
        self.secrets_path = secrets_path
        self.secrets_key = secrets_key
        self.read_secrets()

    def read_config(self):
        self.config = config.read_config.execute(self.config_path)
        return self.config

    def get_config(self):
        return self.config

    def read_secrets(self):
        secrets = config.read_config.execute(self.secrets_path)
        self.secrets = config.decrypt_secrets.execute(self.secrets_key, secrets)
        return self.secrets

    def get_secrets(self):
        return self.secrets

    def connect(self):
        self.set_connection_string()
        self.set_connection_args()
        results = connect.execute(self.config, self.secrets)
        self.connection_code = results['conn_code']
        self.connection_message = results['conn_message']
        if self.connection_code == 'success':
            self.connection = results['connection']
        else:
            self.connection = None
        return self.connection

    def set_connection_string(self):
        dbuser = self.secrets['dbuser']
        dbpassword = self.secrets['dbpassword']
        dbserver = self.config['dbserver']
        dbport = self.config['dbport']
        dbname = self.config['dbname']
        self.connection_string = f'''{dbuser}:{dbpassword}@{dbserver}:{dbport}/{dbname}'''

    def get_connection_string(self):
        return self.connection_string

    def set_connection_args(self):
        self.sslmode = self.config['sslmode']
        self.sslrootcert = self.config['sslrootcert']
        self.defaultschema = self.config['defaultschema']

    def get_sslmode(self):
        return self.sslmode

    def get_sslrootcert(self):
        return self.sslrootcert

    def get_defaultschema(self):
        return self.defaultschema

    def get_connection_code(self):
        return self.connection_code

    def get_connection_message(self):
        return self.connection_message

    def get_connection(self):
        return self.connection

    def run_sql_df(self, sql):
        return run_sql_df.execute(self.connection, sql)

    def extract_ddl(self, schemaname, tablename):
        return extract_ddl.execute(self, schemaname, tablename)

    def rs_stats(self, schemaname, tablename, partition='', whereclause=''):
        return stats.execute(self, schemaname, tablename, partition, whereclause)

    @staticmethod
    def read_sql_file(sqlpath, filename):
        return read_sql_file.execute(sqlpath, filename)

    @staticmethod
    def convert_ddl(ddl):
        return convert_ddl.execute(ddl)

    def set_search_path_schema(self, schemalist):
        return set_search_path_schema.execute(schemalist)

    def list_tables(self, schemaname):
        return list_tables.execute(self.connection, schemaname)

    def unload_sql(self, schemaname, tablename, partition='', whereclause=''):
        return unload_sql.execute(
            self.config,
            self.secrets,
            schemaname,
            tablename,
            partition,
            whereclause)

    def unload_template(
            self,
            config,
            secrets,
            sql,
            s3location,
            unloaddelimiter='\t',
            nullcharacter='<N>',
            addquotes='ADDQUOTES',
            header='HEADER',
            escape='ESCAPE',
            unloadformat='GZIP',
            filesize='5GB',
            parallel='OFF'):
        return unload_template.execute(
            self.config,
            self.secrets,
            sql,
            s3location,
            unloaddelimiter='\t',
            nullcharacter='<N>',
            addquotes='ADDQUOTES',
            header='HEADER',
            escape='ESCAPE',
            unloadformat='GZIP',
            filesize='5GB',
            parallel='OFF')

    def commit(self):
        self.connection.commit()
