#!/usr/bin/env python3


# dependent packages

from psycopg2.extras import NamedTupleCursor


# runs redshift sql and returns cursor

def execute(connection, sql):
    with connection.cursor(cursor_factory=NamedTupleCursor) as cur:
        cur(sql)
        return 0
