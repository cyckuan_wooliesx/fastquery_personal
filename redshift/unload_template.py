#!/usr/bin/env python3


# dependent packages

import util
from datetime import datetime

from . import set_search_path_schema


sql_path = 'sql/unload_template.sql'


def execute(
        config,
        secrets,
        sql,
        s3location,
        unloaddelimiter='\t',
        nullcharacter='<N>',
        addquotes='ADDQUOTES',
        header='HEADER',
        escape='ESCAPE',
        unloadformat='GZIP',
        filesize='5GB',
        parallel='OFF'):

    # generate dynamic sql

    if filesize == '':
        maxfilesize = ''
    else:
        maxfilesize = 'MAXFILESIZE AS ' + filesize

    with open(sql_path, 'r') as sql_file:
        sql = sql_file.read()

    sql = set_search_path_schema.execute([schemaname]) + \
        '\n' + eval('f"""' + sql + '"""')

    print(sql)

    return sql
