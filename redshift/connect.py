#!/usr/bin/env python3


# dependent packages

import psycopg2
import psycopg2.extensions


# get connection

def execute(config, secrets):

    results = {}
    results['conn_message'] = ''
    try:
        conn = psycopg2.connect(
            database=config['dbname'],
            user=config['dbuser'],
            password=secrets['dbpassword'],
            host=config['dbserver'],
            port=config['dbport'],
            sslmode=config['sslmode'],
            sslrootcert=config['sslrootcert'],
            options='-c statement_timeout=36000000')
    except psycopg2.OperationalError as e:
        results['conn_code'] = 'error'
        results['conn_message'] = e.pgerror
    except psycopg2.Error as e:
        results['conn_code'] = 'error'
        results['conn_message'] = e.pgcode + '\n' + e.pgerror
    else:
        results['conn_code'] = 'success'
        results['connection'] = conn

    return results

# psycopg2 async mode not operational
