#!/usr/bin/env python3


# list tables of specified schema

def execute(schemalist):
    sql = ', '.join(schemalist)
    sql = "SET search_path to '$user', public, " + sql + ";"
    return sql
