#!/usr/bin/env python3


# dependent packages

from ddlparse import DdlParse


# convert ddl to bigquery schema json

def execute(ddl):
    parser = DdlParse()

    try:
        table = parser.parse(
            ddl=ddl, source_database=DdlParse.DATABASE.redshift)
    except BaseException:
        return ''
    else:
        return table.to_bigquery_fields()
