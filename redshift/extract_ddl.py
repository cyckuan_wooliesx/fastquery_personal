#!/usr/bin/env python3


# dependent packages

from . import extract_ddl_sql


# extract ddl of specified schema.table

def execute(connection, schemaname, tablename):

    fsql = extract_ddl_sql.execute(schemaname, tablename)
    ddl = connection.run_sql_df(fsql)

    return ddl
