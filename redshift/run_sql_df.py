#!/usr/bin/env python3


# dependent packages

import datetime
import pandas as pd
import psycopg2
import psycopg2.extensions
import sys

from psycopg2.extras import NamedTupleCursor
from collections import Counter

import re

from cleancode import remove_sql_comments



def unique_columns(colnames):

    colhist = Counter(colnames)

    colnames_unique = []
    i = 0
    for c in colnames:
        if colhist[c] > 1:
            i += 1
            colnames_unique.append(c + str(i))
        else:
            i = 0
            colnames_unique.append(c)

    return colnames_unique


# runs redshift sql and returns df

def execute(connection, sql):
    results = {}
    results['sql_message'] = ''

    sql = remove_sql_comments.execute(sql)
    sql = sql.replace('\n', ' ')
    sql = sql.split(' ')
    sql = [w for w in sql if w != '']
    sql = ' '.join(sql)

    with connection.cursor(cursor_factory=NamedTupleCursor) as cur:
        try:
            results['start_time'] = datetime.datetime.now()
            cur.execute(sql)
            if cur.description is not None:
                colnames = [desc[0] for desc in cur.description]
                colnames = unique_columns(colnames)
            else:
                colnames = []
            connection.commit()
        except psycopg2.Error as e:
            results['sql_code'] = 'error'
            results['sql_message'] = e.pgcode + '\n' + e.pgerror
        # except psycopg2.InterfaceError as e:
        else:
            try:
                raw = cur.fetchall()
                if cur.description is not None:
                    results['df'] = pd.DataFrame(raw, columns=colnames)
                else:
                    results['df'] = None
            except psycopg2.ProgrammingError as e:
                results['sql_code'] = 'success'
                results['df'] = None
            except psycopg2.Error as e:
                results['sql_code'] = 'error'
                results['sql_message'] = e.pgcode + '\n' + e.pgerror
            except BaseException:
                results['sql_code'] = 'error'
                results['sql_message'] = str(sys.exc_info()[1])
            else:
                results['sql_code'] = 'success'

    results['end_time'] = datetime.datetime.now()
    elapsed = results['end_time'] - results['start_time']
    results['elapsed'] = elapsed.total_seconds()

    return results
