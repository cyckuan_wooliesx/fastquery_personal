#!/usr/bin/env python3

from . import set_search_path_schema
from . import run_sql_list


def execute(connection, schemaname):

    # generate dynamic sql
    sql = set_search_path_schema.set_search_path_schema(
        [schemaname]) + " SELECT DISTINCT tablename FROM PG_TABLE_DEF WHERE schemaname = '" + schemaname + "';"
    tables = run_sql_list.execute(connection, sql)
    return tables
