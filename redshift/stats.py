#!/usr/bin/env python3


# dependent packages

import os
from datetime import datetime

from . import set_search_path_schema


# compose unload string of schema.table

def execute(
        connection,
        schemaname,
        tablename,
        partition='',
        whereclause=''):

    # generate dynamic sql

    if partition == '':
        partition = datetime.today().strftime('%Y-%m-%d')

    if whereclause != '':
        whereclause = 'where ' + whereclause

    sql_path = os.path.join(os.path.dirname(__file__), 'sql/count.sql')
    with open(sql_path, 'r') as sql_file:
        sql = sql_file.read()

    fsql = eval('f"""' + sql + '"""')

    stats = connection.run_sql_df(fsql)
    stats = stats['df']

    return stats
