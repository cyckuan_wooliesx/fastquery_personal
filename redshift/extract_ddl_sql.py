#!/usr/bin/env python3


# dependent packages

import os


# extract ddl of specified schema.table

def execute(schemaname, tablename):

    schemaname = schemaname.lower()
    tablename = tablename.lower()

    # generate dynamic sql

    sql_path = os.path.join(os.path.dirname(__file__), 'sql/extract_ddl.sql')
    with open(sql_path, 'r') as sql_file:
        sql = sql_file.read()

    fsql = sql + f"""WHERE lower(schemaname) = '{schemaname}' AND lower(tablename) = '{tablename}' LIMIT 10000"""

    return fsql
