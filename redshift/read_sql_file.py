#!/usr/bin/env python3


import os


# compose unload string of schema.table

def execute(sqlpath, filename):

    # generate dynamic sql

    with open(os.path.join(sqlpath, filename), 'r') as sql_file:
        sql = sql_file.read()

    return sql
