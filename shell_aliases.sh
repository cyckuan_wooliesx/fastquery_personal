#!/usr/bin/env bash

lq() {
  cd $scaffold

  if [ -z "$1" ]
  then
    $lqcmd -d re -k prod -c $sys -s debug -q rs
  elif [ -z "$2" ]
  then
    $lqcmd -d re -k prod -c $sys -s $1 -q rs
  else
    $lqcmd -d "$1" -k prod -c $sys -s $2 -q rs
  fi
}

lqb() {
  cd $scaffold

  if [ -z "$1" ]
  then
    $lqcmd -d bwsre -k prod -c $sys -s prod_config -q rs
  else
    $lqcmd -d bwsre -k prod -c $sys -s prod_$1 -q rs
  fi
}

lqgb() {
  cd $scaffold

  if [ -z "$1" ]
  then
    $lqcmd -d bwsre -k prod -c $sys -s prod_config -q gbq
  else
    $lqcmd -d bwsre -k prod -c $sys -s prod_$1 -q gbq
  fi
}

lqs() {
  cd $scaffold

  if [ -z "$1" ]
  then
    $lqcmd -d supre -k prod -c $sys -s prod_config -q rs
  else
    $lqcmd -d supre -k prod -c $sys -s prod_$1 -q rs
  fi
}

lqgs() {
  cd $scaffold

  if [ -z "$1" ]
  then
    $lqcmd -d supre -k prod -c $sys -s prod_config -q gbq
  else
    $lqcmd -d supre -k prod -c $sys -s prod_$1 -q gbq
  fi
}

lqss() {
  cd $scaffold

  if [ -z "$1" ]
  then
    $lqcmd -d supre -k prod -c $sys -s prod_config -q spark
  else
    $lqcmd -d supre -k prod -c $sys -s prod_$1 -q spark
  fi
}

ltb() {
  cd $scaffold

  if [ -z "$1" ]
  then
    $lqcmd -d bwsre -k prod -c $sys -s prod_config -q rs -m translate
  else
    $lqcmd -d bwsre -k prod -c $sys -s prod_$1 -q rs -m translate
  fi
}





lqq() {
  ps aux | grep -v grep | grep main_livequery
}

lqqq() {
  for pid in $(ps aux | grep -v grep | grep main_livequery | cut -b 9-14); do kill -9 $pid; done
}





# sql folders


ssq() {
  if [ -z "$1" ]
  then
    cd $scaffold/sql_debug
  else
    cd $scaffold/sql_$1
  fi
  ls
}

ssb() {
  if [ -z "$1" ]
  then
    cd $scaffold/sql_prod_config
  else
    cd $scaffold/sql_prod_$1
  fi
  ls
}

ssp() {
  if [ -z "$1" ]
  then
    cd $scaffold/sql_prod_config
  else
    cd $scaffold/sql_prod_$1
  fi
  ls
}



