#!/usr/bin/env python3


def execute(config_file):

  with open(config_file,'r') as f:
    tables = f.read().splitlines()

  tables = [t.strip().split('|') for t in tables if len(t.strip()) > 0]

  return tables



