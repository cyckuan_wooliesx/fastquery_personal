#!/usr/bin/env python3


import os
import argparse
import redshift
import gcloud
import util
from datetime import datetime, timedelta

from .class_carryall import carryall
from . import constants
from . import read_params
from . import stop_code
from . import call_process


def execute(config_path, secrets_path, secrets_key, process_name, process, tablelist_path, schemaname='', dataset='', loadmode='noreplace', async_mode=0, partition_flag=1):

  # log file

  main_path = os.path.dirname(tablelist_path)
  logfile = os.path.join(main_path, constants.LOG_PATH,  process_name + '_' + os.path.basename(tablelist_path) + '.log')

  print(logfile)

  # redshift config

  r = redshift.class_connection.connection(config_path, secrets_path, secrets_key)
  r.connect()
  r_config = r.get_config()

  # gcloud config

  g = gcloud.class_connection.connection(config_path, secrets_path, secrets_key)
  g_config = g.get_config()

  # secrets

  s = g.get_secrets()

  # figlet title

  util.figlet_title.execute(r_config['titlefont'], process_name)

  # get table list

  tables = read_params.execute(tablelist_path)

  # init results files

  log_success = tablelist_path + '.success'
  open(log_success, 'w').close()

  log_fail = tablelist_path + '.fail'
  open(log_fail, 'w').close()

  # table iterator

  stop = 0

  for table in tables:

    src_tablename = table[0]


    # stop

    stop = stop_code.execute(constants.STOP_CODE_PATH)
    if stop > 0:
      break


    # param list management

    if len(table) > 2:
      dest_tablename = table[1]
      partition = table[2]
      pl = partition.split(':')
      whereclause_template = table[3]

    elif len(table) > 1:
      dest_tablename = table[1]
      partition = datetime.today().strftime('%Y-%m-%d')
      pl = ['na']
      whereclause = ''

    else:
      dest_tablename = src_tablename
      partition = datetime.today().strftime('%Y-%m-%d')
      pl = ['na']
      whereclause = ''


    # date iterator

    if pl[0] == 'daterange' and partition_flag > 0:

      date_from = datetime.strptime(pl[1],'%Y-%m-%d')
      date_to = datetime.strptime(pl[2],'%Y-%m-%d')

      day = timedelta(days=1)

      print(pl)

      # iterative call

      while date_from <= date_to and stop == 0:

        partition = date_from.strftime('%Y-%m-%d')
        whereclause = whereclause_template.replace('$date',partition)

        ca = carryall(r_connection=r, g_connection=g, secrets=s, dataset=dataset, schemaname=schemaname, src_tablename=src_tablename, dest_tablename=dest_tablename, partition=partition, whereclause=whereclause, loadmode=loadmode, process=process, logfile=logfile, main_path=main_path, tablelist_path=tablelist_path)
        call_process.execute(ca, async_mode)

        date_from = date_from + day

    else:

      # single call

      ca = carryall(r_connection=r, g_connection=g, secrets=s, dataset=dataset, schemaname=schemaname, src_tablename=src_tablename, dest_tablename=dest_tablename, partition=partition, whereclause=whereclause, loadmode=loadmode, process=process, logfile=logfile, main_path=main_path, tablelist_path=tablelist_path)
      call_process.execute(ca, async_mode)


  if stop > 0:
    print(fg.red + 'STOPPED' + fg.cancel)


  return 0





