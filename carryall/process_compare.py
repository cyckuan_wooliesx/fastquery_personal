#!/usr/bin/env python3


import pandas as pd


def execute(ca):


  r_stats = ca.r_connection.rs_stats(ca.schemaname, ca.src_tablename, ca.partition, ca.whereclause)

  g_stats = ca.g_connection.bq_stats(ca.dataset, ca.dest_tablename)

  z_stats = pd.concat([r_stats, g_stats], axis=1)
  z_stats.columns = ['redshift','bigquery']
  z_stats['diff'] = z_stats['redshift'] - z_stats['bigquery']


  if z_stats['diff'][0] == 0:
    logf = ca.tablelist_path + '.success'
  else:
    logf = ca.tablelist_path + '.fail'

  with open(logf,'a') as lf:
    lf.write(ca.src_tablename+'\n')

  print(z_stats)


