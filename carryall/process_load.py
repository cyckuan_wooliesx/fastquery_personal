#!/usr/bin/env python3


import os
from . import constants


def execute(ca):

  config = ca.g_connection.get_config()

  rs_schematable = ca.schemaname + '.' + ca.src_tablename
  rs_schemapath = os.path.join(ca.main_path, constants.JSON_OUTPUT_PATH, rs_schematable + '.json')
  datafiles = os.path.join(config['gsrepo'], config['dataprefix'], ca.schemaname, ca.src_tablename, ca.partition, '*.gz')

  ca.g_connection.load_csv_bq(ca.dataset, ca.dest_tablename, ca.schemaname, ca.src_tablename, datafiles, rs_schemapath, ca.loadmode)



