#!/usr/bin/env python3

import os
from . import constants


def execute(ca):

  bq_schematable = ca.dataset + '.' + ca.dest_tablename
  rs_schematable = ca.schemaname + '.' + ca.src_tablename
  rs_schemapath = os.path.join(ca.main_path, constants.JSON_OUTPUT_PATH, rs_schematable + '.json')

  ca.g_connection.bq_delete_table(bq_schematable)
  ca.g_connection.bq_create_table(bq_schematable, rs_schemapath)




