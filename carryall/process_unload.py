#!/usr/bin/env python3


def execute(ca):

  sql = ca.r_connection.unload_sql(ca.schemaname, ca.src_tablename, ca.partition, ca.whereclause)

  ca.r_connection.run_sql_df(sql)


