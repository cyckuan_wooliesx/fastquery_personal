#!/usr/bin/env python3


def execute(ca):

  config = ca.g_connection.get_config()

  src = '/'.join([config['s3repo'], config['dataprefix'], ca.schemaname, ca.src_tablename, ca.partition]) + '/'
  dest = '/'.join([config['gsrepo'], config['dataprefix'], ca.schemaname, ca.src_tablename, ca.partition]) + '/'

  ca.g_connection.rsync(src, dest)


