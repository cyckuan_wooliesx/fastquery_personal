#!/usr/bin/env python3


import os
from . import constants


def execute(ca):

  print(ca.schemaname)

  # get redshift table ddl
  ddl = ca.r_connection.extract_ddl(ca.schemaname, ca.src_tablename)
  ddl = '\n'.join(ddl['df']['ddl'].tolist())

  print(ddl)

  # write ddl to file
  with open(os.path.join(ca.main_path, constants.DDL_OUTPUT_PATH, ca.schemaname + '.' + ca.src_tablename + '.sql'), "w") as text_file:
    text_file.write(ddl)

  # convert ddl to bigquery schema json
  json_schema = ca.r_connection.convert_ddl(ddl)

  print(json_schema)

  # write json to file
  with open(os.path.join(ca.main_path, constants.JSON_OUTPUT_PATH, ca.schemaname + '.' + ca.src_tablename + '.json'), "w") as text_file:
    text_file.write(json_schema)



