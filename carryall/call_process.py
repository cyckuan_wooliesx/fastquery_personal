#!/usr/bin/env python3

import util

from multiprocessing import Process
from sty import fg, bg, ef, rs


def execute(ca, async_mode=0):

  print(fg.green + '\n' + ca.src_tablename + '\n' + ca.partition + '\n' + fg.rs)
  util.logentry.execute(ca.logfile, ca.src_tablename + ' : ' + ca.partition)

  if async_mode > 0:
    p = Process(target=ca.process, args=(ca,))
    p.start()
  else:
    ca.process(ca)





